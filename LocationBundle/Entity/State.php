<?php

namespace Potato\LocationBundle\Entity;

use Potato\LocationBundle\Model\State as BaseState;

/**
 * Class State
 */
class State extends BaseState
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var \Potato\LocationBundle\Model\City[]
     */
    protected $cities = array();

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}