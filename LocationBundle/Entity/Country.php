<?php

namespace Potato\LocationBundle\Entity;

use Potato\LocationBundle\Model\Country as BaseCountry;

/**
 * Class Country
 */
class Country extends BaseCountry
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var \Potato\LocationBundle\Model\City[]
     */
    protected $cities = array();

    /**
     * @var \Potato\LocationBundle\Model\State[]
     */
    protected $states = array();

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}