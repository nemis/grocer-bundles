<?php

namespace Potato\LocationBundle\Entity;

use Potato\LocationBundle\Model\City as BaseCity;

/**
 * Class City
 */
class City extends BaseCity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}