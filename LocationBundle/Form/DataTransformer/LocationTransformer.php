<?php

namespace Potato\LocationBundle\Form\DataTransformer;

use Potato\LocationBundle\Model\Factory\LocationFactory;
use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Services\Finder;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Serializer\SerializerInterface;

class LocationTransformer implements DataTransformerInterface
{
    /**
     * @var \Potato\LocationBundle\Model\Factory\LocationFactory
     */
    protected $locationFactory;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var \Potato\LocationBundle\Services\Finder
     */
    protected $finder;

    /**
     * @var bool
     */
    protected $revise;

    /**
     * @param SerializerInterface $serializer
     * @param Finder $finder
     * @param LocationFactory $locationFactory
     * @param bool $revise
     */
    public function __construct(
        SerializerInterface $serializer,
        Finder $finder,
        LocationFactory $locationFactory,
        $revise = false
    ) {
        $this->serializer = $serializer;
        $this->finder = $finder;
        $this->locationFactory = $locationFactory;
        $this->revise = $revise;
    }

    /**
     * @param string $data
     * @return \Potato\LocationBundle\Model\Location
     */
    protected function jsonToLocation($data)
    {
        $data = str_replace('&quot;', '"', $data);
        if (!empty($data)) {
            return $this->locationFactory->createFromJson($data);
        }
    }

    /**
     * @param \Potato\LocationBundle\Model\Location $data
     * @return string
     * @throws TransformationFailedException
     */
    protected function locationToJson($data)
    {
        if (empty($data)) {
            return "";
        }

        if (!$data instanceof Location) {
            throw new TransformationFailedException();
        }

        return $this->serializer->serialize($data, 'json');
    }

    /**
     * @param string $data
     * @throws TransformationFailedException
     * @return Location
     */
    public function reverseTransform($data)
    {
        $location = $this->jsonToLocation($data);

        if ($this->revise) {
            $this->finder->reviseLocation($location);
        }

        return $location;
    }

    /**
     * @param Location|mixed $data
     * @throws TransformationFailedException
     * @return string
     */
    public function transform($data)
    {
        return $this->locationToJson($data);
    }
}