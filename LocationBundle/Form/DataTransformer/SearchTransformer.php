<?php

namespace Potato\LocationBundle\Form\DataTransformer;

use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Services\Finder;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class SearchTransformer
 * @package Potato\LocationBundle\Form\DataTransformer
 */
class SearchTransformer implements DataTransformerInterface
{
    /**
     * @var \Potato\LocationBundle\Services\Finder
     */
    private $finder;

    /**
     * @param Finder $finder
     */
    public function __construct(Finder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param mixed $data
     * @return mixed|void
     */
    public function reverseTransform($data)
    {
        $location = null;

        if (!empty($data)) {
            $results = $this->finder->translateString($data);

            if (!empty($results)) {
                $location = array_shift($results);

                if ($location instanceof Location) {
                    $location = $location->getCoordinates()->getLatitude()
                        .','
                        .$location->getCoordinates()->getLongitude();
                }
            }
        }

        return $location;
    }

    /**
     * @param mixed $data
     * @return mixed|void
     */
    public function transform($data)
    {
        if ($data instanceof Location) {
            return (string)$data;
        }

        return $data;
    }
}
