<?php

namespace Potato\LocationBundle\Form\DataTransformer;

use Potato\LocationBundle\Adapter\LocationEntity;
use Potato\LocationBundle\Model\Factory\LocationFactory;
use Potato\LocationBundle\Services\Finder;
use Potato\LocationBundle\Services\Persistence\LocationPersistenceInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Serializer\SerializerInterface;

class PersistentLocationTransformer extends LocationTransformer
{
    /**
     * @var \Potato\LocationBundle\Services\Persistence\LocationPersistenceInterface
     */
    protected $locationPersistence;

    /**
     * @param SerializerInterface $serializer
     * @param LocationFactory $locationFactory
     * @param Finder $finder
     * @param LocationPersistenceInterface $locationPersistence
     */
    public function __construct(
        SerializerInterface $serializer,
        LocationFactory $locationFactory,
        Finder $finder,
        LocationPersistenceInterface $locationPersistence
    ) {
        parent::__construct($serializer, $finder, $locationFactory);

        $this->locationPersistence = $locationPersistence;
    }

    /**
     * @param string $data
     * @return \Potato\LocationBundle\Adapter\LocationEntity
     * @throws TransformationFailedException
     */
    protected function jsonToLocation($data)
    {
        $location = parent::jsonToLocation($data);

        if (!$location) {
            throw new TransformationFailedException();
        }

        return new LocationEntity($this->locationPersistence, $location);
    }
}