<?php

namespace Potato\LocationBundle\Form\DataTransformer;

use Potato\LocationBundle\Adapter\AddressEntity;
use Potato\LocationBundle\Model\Factory\LocationFactory;
use Potato\LocationBundle\Services\Finder;
use Potato\LocationBundle\Services\Persistence\LocationPersistenceInterface;

class PersistentAddressTransformer extends AddressTransformer
{
    /**
     * @var \Potato\LocationBundle\Services\Persistence\LocationPersistenceInterface
     */
    protected $locationPersistence;

    /**
     * @param LocationFactory $locationFactory
     * @param LocationPersistenceInterface $locationPersistence
     * @param Finder $finder
     * @param bool $revise
     */
    public function __construct(
        LocationFactory $locationFactory,
        Finder $finder,
        LocationPersistenceInterface $locationPersistence,
        $revise = false
    ) {
        parent::__construct($locationFactory, $finder, $revise);

        $this->locationPersistence = $locationPersistence;
    }

    /**
     * @param string $data
     * @return \Potato\LocationBundle\Adapter\AddressEntity
     */
    public function reverseTransform($data)
    {
        $address = parent::reverseTransform($data);

        return new AddressEntity($this->locationPersistence, $address);
    }
}