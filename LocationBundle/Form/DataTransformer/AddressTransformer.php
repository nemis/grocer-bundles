<?php

namespace Potato\LocationBundle\Form\DataTransformer;

use Potato\LocationBundle\Model\Factory\LocationFactory;
use Potato\LocationBundle\Model\Address;
use Potato\LocationBundle\Services\Finder;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class AddressTransformer implements DataTransformerInterface
{
    /**
     * @var LocationFactory
     */
    protected $factory;

    /**
     * @var bool
     */
    protected $revise;

    /**
     * @var \Potato\LocationBundle\Services\Finder
     */
    protected $finder;

    /**
     * @param LocationFactory $factory
     * @param Finder $finder
     * @param bool $revise
     */
    public function __construct(LocationFactory $factory, Finder $finder, $revise = false)
    {
        $this->factory = $factory;
        $this->finder = $finder;
        $this->revise = $revise;
    }

    /**
     * @param Address|mixed $data
     * @throws TransformationFailedException
     * @return array
     */
    public function transform($data)
    {
        $result = array_fill_keys(array('city', 'state', 'country', 'firstLine', 'secondLine', 'zip'), '');

        if (empty($data)) {
            return $result;
        }

        if (!$data instanceof Address) {
            throw new TransformationFailedException('Data is not an address object.');
        }

        $result['city'] = $data->getLocation()->getCity()->getName();
        $result['state'] = $data->getLocation()->getCity()->getState()->getName();
        $result['stateShortName'] = $data->getLocation()->getCity()->getState()->getShortName();
        $result['country'] = $data->getLocation()->getCity()->getState()->getCountry()->getName();
        $result['countryShortName'] = $data->getLocation()->getCity()->getState()->getCountry()->getShortName();
        $result['firstLine'] = $data->getFirstLine();
        $result['secondLine'] = $data->getSecondLine();
        $result['zip'] = $data->getZip();

        return $result;
    }

    /**
     * @param mixed $data
     * @throws TransformationFailedException
     * @return Address
     */
    public function reverseTransform($data)
    {
        $address = $this->factory->createFromArray($data);

        if ($this->revise) {
            $this->finder->reviseLocation($address->getLocation());
        }

        return $address;
    }
}