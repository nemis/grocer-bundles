<?php

namespace Potato\LocationBundle\Form\Type;

use Potato\LocationBundle\Form\DataTransformer\LocationTransformer;
use Potato\LocationBundle\Form\DataTransformer\SearchTransformer;
use Potato\LocationBundle\Validator\Constraints\ValidLocation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SearchType
 */
class SearchType extends AbstractType
{
    /**
     * @var \Potato\LocationBundle\Form\DataTransformer\SearchTransformer
     */
    protected $searchTransformer;

    /**
     * @param SearchTransformer $searchTransformer
     */
    public function __construct(SearchTransformer $searchTransformer)
    {
        $this->searchTransformer = $searchTransformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer($this->searchTransformer);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'potato_location_search_type';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }
}
