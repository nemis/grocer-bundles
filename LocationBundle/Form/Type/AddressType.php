<?php

namespace Potato\LocationBundle\Form\Type;

use Potato\LocationBundle\Form\DataTransformer\AddressTransformer;
use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Validator\Constraints\ValidAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

/**
 * Class AddressType
 */
class AddressType extends AbstractType
{
    /**
     * @var \Potato\LocationBundle\Form\DataTransformer\AddressTransformer
     */
    protected $locationTransformer;

    /**
     * @param AddressTransformer $locationTransformer
     */
    public function __construct(AddressTransformer $locationTransformer)
    {
        $this->locationTransformer = $locationTransformer;
    }

    /**
     * @param array $options
     * @return array
     * @throws \InvalidArgumentException
     */
    private function getEmptyData(array $options)
    {
        $keys = array(
            'city', 'state', 'country'
        );

        $result = array_fill_keys($keys, '');

        if (isset($options['location'])) {
            $location = $options['location'];

            if (!$location instanceof Location) {
                throw new \InvalidArgumentException('Given location option is not valid location object.');
            }

            $result['city'] = $location->getCity()->getName();
            $result['state'] = $location->getCity()->getState()->getName();
            $result['country'] = $location->getCity()->getState()->getCountry()->getName();
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this->locationTransformer);

        $emptyData = $this->getEmptyData($options);

        $builder->add('firstLine', 'text', array('label' => 'form.address.first_line'));
        $builder->add('secondLine', 'text', array('label' => 'form.address.second_line'));
        $builder->add('city', 'text', array('label' => 'form.address.city', 'empty_data' => $emptyData['city']));
        $builder->add('state', 'text', array('label' => 'form.address.state', 'empty_data' => $emptyData['state']));

        $builder->add(
            'country',
            'text',
            array('label' => 'form.address.country', 'empty_data' => $emptyData['country'])
        );

        $builder->add('zip', 'text', array('label' => 'form.address.zip'));
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $view->vars['uniqid'] = uniqid();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'potato_address_type';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'compound' => true,
            'constraints' => array(
                new ValidAddress()
            ),
            'location' => null
        ));
    }

    /**
     * {@inheritodc}
     */
    public function getParent()
    {
        return 'form';
    }
}