<?php

namespace Potato\LocationBundle\Form\Type;

/**
 * Class PersistentLocationType
 */
class PersistentLocationType extends LocationType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'potato_location_persistent_type';
    }
}