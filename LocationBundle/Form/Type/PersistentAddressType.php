<?php

namespace Potato\LocationBundle\Form\Type;

/**
 * Class PersistentAddressType
 */
class PersistentAddressType extends AddressType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'potato_address_persistent_type';
    }
}