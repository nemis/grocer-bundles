<?php

namespace Potato\LocationBundle\Form\Type;

use Potato\LocationBundle\Form\DataTransformer\LocationTransformer;
use Potato\LocationBundle\Validator\Constraints\ValidLocation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class LocationType
 */
class LocationType extends AbstractType
{
    /**
     * @var \Potato\LocationBundle\Form\DataTransformer\LocationTransformer
     */
    protected $locationTransformer;

    /**
     * @param LocationTransformer $locationTransformer
     */
    public function __construct(LocationTransformer $locationTransformer)
    {
        $this->locationTransformer = $locationTransformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this->locationTransformer);
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $view->vars['uniqid'] = uniqid();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'potato_location_type';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'constraints' => array(
                new ValidLocation()
            )
        ));
    }
}