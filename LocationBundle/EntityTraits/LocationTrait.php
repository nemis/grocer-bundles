<?php

namespace Potato\LocationBundle\EntityTraits;

use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Model\Value\Coordinates;
use Potato\LocationBundle\Adapter\LocationEntity;

trait LocationTrait
{
    /**
     * @return Location
     */
    public function getLocation()
    {
        $coordinates = new Coordinates($this->latitude, $this->longitude);

        $location = new Location($this->city, $coordinates);

        return $location;
    }

    /**
     * @param Location $location
     * @throws \InvalidArgumentException
     */
    public function setLocation(Location $location)
    {
        if (!$location instanceof LocationEntity) {
            throw new \InvalidArgumentException('Location must be valid persistence entity.');
        }

        $this->latitude = $location->getCoordinates()->getLatitude();
        $this->longitude = $location->getCoordinates()->getLongitude();

        $this->city = $location->getCity();
    }
}