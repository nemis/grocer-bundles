<?php

namespace Potato\LocationBundle\EntityTraits\DoctrineORMAnnotationMapping;

trait AddressPropertiesTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="\Potato\LocationBundle\Entity\City")
     *
     * @var \Potato\LocationBundle\Entity\City
     */
    private $city;

    /**
     * @ORM\Column(type="float", scale=8)
     */
    private $latitude;

    /**
     * @ORM\Column(type="float", scale=8)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="address_first_line", type="string", length=255)
     */
    private $firstLine;

    /**
     * @var string
     *
     * @ORM\Column(name="address_second_line", type="string", length=255)
     */
    private $secondLine;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=100)
     */
    private $zip;
}