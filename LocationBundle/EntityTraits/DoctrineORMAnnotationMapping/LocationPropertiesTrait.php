<?php

namespace Potato\LocationBundle\EntityTraits\DoctrineORMAnnotationMapping;

trait LocationPropertiesTrait
{
    /**
     * @ORM\Column(type="float", scale=8)
     */
    private $latitude;

    /**
     * @ORM\Column(type="float", scale=8)
     */
    private $longitude;

    /**
     * @ORM\ManyToOne(targetEntity="\Potato\LocationBundle\Entity\City")
     *
     * @var \Potato\LocationBundle\Entity\City
     */
    private $city;
}