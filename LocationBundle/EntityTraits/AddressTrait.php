<?php

namespace Potato\LocationBundle\EntityTraits;

use Potato\LocationBundle\Adapter\AddressEntity;
use Potato\LocationBundle\Model\Address;
use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Model\Value\Coordinates;

trait AddressTrait
{
    /**
     * Set address
     *
     * @param Address $address
     * @throws \InvalidArgumentException
     *
     * @return null
     */
    public function setAddress(Address $address)
    {
        if (!$address instanceof AddressEntity) {
            throw new \InvalidArgumentException('Address must be instance of AddressEntity.');
        }

        $this->firstLine = $address->getFirstLine();
        $this->secondLine = $address->getSecondLine();
        $this->zip = $address->getZip();
        $this->city = $address->getLocation()->getCity();
        $this->latitude = $address->getLocation()->getCoordinates()->getLatitude();
        $this->longitude = $address->getLocation()->getCoordinates()->getLongitude();
    }

    /**
     * @return AddressEntity
     */
    public function getAddress()
    {
        return new Address(
            $this->firstLine,
            $this->secondLine,
            $this->zip,
            new Location($this->city,new Coordinates($this->latitude, $this->longitude))
        );
    }
}