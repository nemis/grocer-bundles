<?php

namespace Potato\LocationBundle\Adapter;

use Potato\LocationBundle\Model\Address;
use Potato\LocationBundle\Services\Persistence\LocationPersistenceInterface;

class AddressEntity extends Address
{
    /**
     * @var Address
     */
    private $addressModel;

    /**
     * @var LocationPersistenceInterface
     */
    private $locationPersistence;

    /**
     * @param LocationPersistenceInterface $locationPersistence
     * @param Address $addressModel
     */
    public function __construct(LocationPersistenceInterface $locationPersistence, Address $addressModel)
    {
        $this->locationPersistence = $locationPersistence;
        $this->addressModel = $addressModel;

        $this->firstLine = $addressModel->getFirstLine();
        $this->secondLine = $addressModel->getSecondLine();
        $this->zip = $addressModel->getZip();

        $this->location = $addressModel->getLocation();

        $this->locationPersistence->persistLocation($this->location, true);
    }
}