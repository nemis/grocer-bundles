<?php

namespace Potato\LocationBundle\Adapter;

use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Services\Persistence\LocationPersistenceInterface;
use Potato\LocationBundle\Model\City;
use Potato\LocationBundle\Model\Value\Coordinates;
use Potato\LocationBundle\Entity\City as CityEntity;

class LocationEntity extends Location
{
    /**
     * @var Location
     */
    private $locationModel;

    /**
     * @var LocationPersistenceInterface
     */
    private $locationPersistence;

    /**
     * @param LocationPersistenceInterface $locationPersistence
     * @param Location $locationModel
     */
    public function __construct(LocationPersistenceInterface $locationPersistence, Location $locationModel)
    {
        $this->locationPersistence = $locationPersistence;
        $this->locationModel = $locationModel;

        $this->locationPersistence->persistLocation($locationModel, true);

        $this->city = $locationModel->getCity();

        $this->coordinates = $locationModel->getCoordinates();
    }

    /**
     * @param \Potato\LocationBundle\Model\City $city
     * @throws \InvalidArgumentException
     */
    public function setCity(City $city)
    {
        if (!$city instanceof CityEntity) {
            throw new \InvalidArgumentException('Value must be a persistence entity.');
        }

        $this->city = $city;
    }

    /**
     * @return \Potato\LocationBundle\Model\Value\Coordinates
     */
    public function getCoordinates()
    {
        return $this->locationModel->getCoordinates();
    }

    /**
     * @param \Potato\LocationBundle\Model\Value\Coordinates
     */
    public function setCoordinates(Coordinates $coordinates)
    {
        $this->locationModel->setCoordinates($coordinates);
    }
}
