<?php

namespace Potato\LocationBundle\Controller;

use Potato\LocationBundle\Form\DataTransformer\LocationTransformer;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FindController extends ContainerAware
{
    public function locationAction(Request $request)
    {
        $location = $request->get('location');

        $locations = $this->container->get('potato.location.finder')->translateString($location);

        $response = new Response($this->container->get('potato.location.serializer')->serialize($locations, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function locationFromLatLangAction(Request $request)
    {
        $locationString = $request->get('location');

        if (strpos($locationString, ',') === false) {
            throw new BadRequestHttpException(
                'Invalid coordinates. The param must have two values separated by a comma.'
            );
        }

        list($lat, $lang) = explode(',', $locationString);

        $locations = $this->container->get('potato.location.finder')->translateLatLang($lat, $lang);

        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $response = new Response($serializer->serialize($locations, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}