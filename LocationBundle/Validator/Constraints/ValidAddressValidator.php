<?php

namespace Potato\LocationBundle\Validator\Constraints;

use Potato\LocationBundle\Model\Address;
use Symfony\Component\Validator\Constraint;

class ValidAddressValidator extends AbstractLocationValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$value instanceof Address || !$this->locationExists($value->getLocation())) {
            $this->context->addViolation(
                $constraint->message,
                array('%string%' => (string) $value)
            );
        }
    }
}