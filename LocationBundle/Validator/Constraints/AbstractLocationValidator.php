<?php

namespace Potato\LocationBundle\Validator\Constraints;

use Potato\LocationBundle\Model\Location;
use Symfony\Component\Validator\ConstraintValidator;
use Potato\LocationBundle\Services\Finder;

abstract class AbstractLocationValidator extends ConstraintValidator
{
    /**
     * @var \Potato\LocationBundle\Services\Finder
     */
    private $finder;

    /**
     * @param Finder $finder
     */
    public function __construct(Finder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param Location $location
     * @return bool
     * @throws \InvalidArgumentException
     */
    protected function locationExists(Location $location)
    {
        $locationsFromFinder = $this->finder->translateString((string)$location);

        foreach ($locationsFromFinder as $locationFromFinder) {
            if ($this->locationsEqual($location, $locationFromFinder)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Location $current
     * @param Location $expected
     * @return bool
     */
    private function locationsEqual(Location $current, Location $expected)
    {
        return ((string)$current == (string)$expected);
    }
}