<?php

namespace Potato\LocationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ValidAddress extends Constraint
{
    public $message = 'The given address is invalid.';

    public function validatedBy()
    {
        return 'potato_validator_address';
    }
}