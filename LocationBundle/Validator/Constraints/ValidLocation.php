<?php

namespace Potato\LocationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ValidLocation extends Constraint
{
    public $message = 'The given location is invalid.';

    public function validatedBy()
    {
        return 'potato_validator_location';
    }
}