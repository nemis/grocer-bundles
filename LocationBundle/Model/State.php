<?php

namespace Potato\LocationBundle\Model;

class State
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $shortName;

    /**
     * @var Country
     */
    protected $country;

    /**
     * @param string $name
     * @param string $shortName
     * @param Country $country
     */
    public function __construct($name = '', $shortName = '', Country $country = null)
    {
        $this->name = $name;
        $this->shortName = $shortName;

        if (is_null($country)) {
            $country = new Country();
        }

        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $shortName
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @return mixed
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @param \Potato\LocationBundle\Model\Country $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return \Potato\LocationBundle\Model\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}