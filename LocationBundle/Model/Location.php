<?php

namespace Potato\LocationBundle\Model;

use Potato\LocationBundle\Model\Value\Coordinates;

/**
 * Value object for location
 *
 * @package Potato\Grocery\LocationBundle\Location\Value
 */
class Location implements CoordinableInterface
{
    /**
     * @var \Potato\LocationBundle\Model\City
     */
    protected $city;

    /**
     * @var \Potato\LocationBundle\Model\Value\Coordinates
     */
    protected $coordinates;

    /**
     * @param City $city
     * @param Coordinates $coordinates
     */
    public function __construct(
        City $city = null,
        Coordinates $coordinates = null
    ) {
        if (is_null($city)) {
            $city = new City();
        }

        if (is_null($coordinates)) {
            $coordinates = new Coordinates();
        }

        $this->city = $city;
        $this->coordinates = $coordinates;
    }

    /**
     * @return \Potato\LocationBundle\Model\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param \Potato\LocationBundle\Model\City $city
     */
    public function setCity(City $city)
    {
        $this->city = $city;
    }

    /**
     * @return \Potato\LocationBundle\Model\Value\Coordinates
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param \Potato\LocationBundle\Model\Value\Coordinates $coordinates
     */
    public function setCoordinates(Coordinates $coordinates)
    {
        $this->coordinates = $coordinates;
    }

    /**
     * Check if value is fully set up
     *
     * @return bool
     */
    public function isFullyDefined()
    {
        $propertiesDefined = count(
            array_filter(
                get_object_vars($this),
                function ($p) {
                    return is_null($p);
                }
            )
        ) == 0;

        $cordsDefined = abs($this->getCoordinates()->getLatitude()) > 0 &&
            abs($this->getCoordinates()->getLongitude()) > 0;

        return ($propertiesDefined && $cordsDefined);
    }

    /**
     * Returns hash for location value object
     *
     * @return string
     */
    public function getSumHash()
    {
        return md5((string)$this);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->city.' '.$this->city->getState().' '.$this->city->getState()->getCountry();
    }
}
