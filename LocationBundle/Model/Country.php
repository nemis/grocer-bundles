<?php

namespace Potato\LocationBundle\Model;

class Country
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $shortName;

    /**
     * @param string $name
     * @param string $shortName
     */
    public function __construct($name = '', $shortName = '')
    {
        $this->name = $name;
        $this->shortName = $shortName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $shortName
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}