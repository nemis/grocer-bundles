<?php

namespace Potato\LocationBundle\Model;

use Potato\LocationBundle\Model\Value\Coordinates;

/**
 * Interface Location
 * @package Potato\LocationBundle\Model
 */
interface CoordinableInterface
{
    /**
     * @return \Potato\LocationBundle\Model\Value\Coordinates
     */
    public function getCoordinates();

    /**
     * @param Coordinates $coordinates
     * @return mixed
     */
    public function setCoordinates(Coordinates $coordinates);

}