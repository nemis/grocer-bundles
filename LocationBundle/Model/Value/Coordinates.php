<?php

namespace Potato\LocationBundle\Model\Value;

class Coordinates
{
    /**
     * @var double|string
     */
    private $longitude;
    /**
     * @var double|string
     */
    private $latitude;

    /**
     * @param double|string $latitude
     * @param double|string $longitude
     */
    public function __construct($latitude = null, $longitude = null)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return float|string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->latitude.', '.$this->longitude;
    }

    /**
     * @param Coordinates $coordinates
     * @return int
     */
    public function getDistanceToInMeters(Coordinates $coordinates)
    {
        $latFrom = deg2rad($this->getLatitude());
        $lonFrom = deg2rad($this->getLongitude());
        $latTo = deg2rad($coordinates->getLatitude());
        $lonTo = deg2rad($coordinates->getLongitude());

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * 6371000; // 6371000 == earth radius in meters
    }
}