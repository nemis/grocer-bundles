<?php

namespace Potato\LocationBundle\Model;

use Potato\LocationBundle\Model\Value\Coordinates;

class Address
{
    /**
     * @var string
     */
    protected $firstLine;

    /**
     * @var string
     */
    protected $secondLine;

    /**
     * @var string
     */
    protected $zip;

    /**
     * @var Location
     */
    protected $location;

    /**
     * @param string $firstLine
     * @param string $secondLine
     * @param string $zip
     * @param Location $location
     */
    public function __construct(
        $firstLine = '',
        $secondLine = '',
        $zip = '',
        Location $location = null
    ) {
        $this->firstLine = $firstLine;
        $this->secondLine = $secondLine;
        $this->zip = $zip;

        if (is_null($location)) {
            $country = new Country();
            $state = new State('', $country);
            $city = new City('', $state, $state);
            $coordinates = new Coordinates();

            $location = new Location($city, $coordinates);
        }

        $this->location = $location;
    }

    /**
     * @param \Potato\LocationBundle\Model\Location $location
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
    }

    /**
     * @return \Potato\LocationBundle\Model\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $firstLine
     */
    public function setFirstLine($firstLine)
    {
        $this->firstLine = $firstLine;
    }

    /**
     * @return string
     */
    public function getFirstLine()
    {
        return $this->firstLine;
    }

    /**
     * @param string $secondLine
     */
    public function setSecondLine($secondLine)
    {
        $this->secondLine = $secondLine;
    }

    /**
     * @return string
     */
    public function getSecondLine()
    {
        return $this->secondLine;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @retun string
     */
    public function __toString()
    {
        return implode(' ', array(
            $this->getFirstLine(),
            $this->getSecondLine(),
            $this->getLocation()->getCity()->getName(),
            $this->getLocation()->getCity()->getState()->getName(),
            $this->getLocation()->getCity()->getState()->getCountry()->getName(),
            $this->getZip()
        ));
    }
}