<?php

namespace Potato\LocationBundle\Model\Factory;

use Potato\LocationBundle\Model\City;
use Potato\LocationBundle\Model\Country;
use Potato\LocationBundle\Model\State;
use Potato\LocationBundle\Model\Value\Coordinates;
use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Model\Factory\LocationCreationStrategy\CreationStrategyInterface;

/**
 * Class LocationFactory
 *
 * creates location value object based on given data
 *
 * @package Potato\LocationBundle\Model\Value\Factory
 */
class LocationFactory
{
    /**
     * @var LocationCreationStrategy\CreationStrategyInterface
     */
    private $creationStrategy;

    /**
     * @param CreationStrategyInterface $creationStrategy
     */
    public function __construct(CreationStrategyInterface $creationStrategy)
    {
        $this->creationStrategy = $creationStrategy;
    }

    /**
     * @param array $array
     * @param string $key
     * @param bool $ensureExists
     * @return mixed
     * @throws \OutOfBoundsException
     */
    private function getArrayEntry($array, $key, $ensureExists = true)
    {
        if (!array_key_exists($key, $array)) {
            if ($ensureExists) {
                throw new \OutOfBoundsException('Missing '.$key.'.');
            } else {
                $array[$key] = '';
            }
        }

        return $array[$key];
    }

    /**
     * @param array $data
     * @return Location
     */
    public function createFromArray(array $data)
    {
        $country = new Country(
            $this->getArrayEntry($data, 'country'),
            $this->getArrayEntry($data, 'countryShortName', false)
        );

        $state = new State(
            $this->getArrayEntry($data, 'state'),
            $this->getArrayEntry($data, 'stateShortName', false),
            $country
        );

        $city = new City($this->getArrayEntry($data, 'city'), $state, $country);

        $coordinates =  new Coordinates(
            $this->getArrayEntry($data, 'latitude', false),
            $this->getArrayEntry($data, 'longitude', false)
        );

        return $this->creationStrategy->create($data, $city, $coordinates);
    }

    /**
     * @param string $json
     * @return Location
     */
    public function createFromJson($json)
    {
        return $this->createFromNestedArray(json_decode($json, true));
    }

    /**
     * @param array $data
     * @return Location
     */
    public function createFromNestedArray(array $data)
    {
        $cityArray = $this->getArrayEntry($data, 'city');
        $stateArray = $this->getArrayEntry($cityArray, 'state');
        $countryArray = $this->getArrayEntry($stateArray, 'country');
        $coordinatesArray = $this->getArrayEntry($data, 'coordinates');

        $country = new Country(
            $this->getArrayEntry($countryArray, 'name'),
            $this->getArrayEntry($countryArray, 'shortName')
        );

        $state = new State(
            $this->getArrayEntry($stateArray, 'name'),
            $this->getArrayEntry($stateArray, 'shortName'),
            $country
        );

        $city = new City($this->getArrayEntry($cityArray, 'name'), $state, $country);

        $coordinates =  new Coordinates(
            $this->getArrayEntry($coordinatesArray, 'latitude', false),
            $this->getArrayEntry($coordinatesArray, 'longitude', false)
        );

        return $this->creationStrategy->create($data, $city, $coordinates);
    }
}