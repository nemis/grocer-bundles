<?php

namespace Potato\LocationBundle\Model\Factory\LocationCreationStrategy;

use Potato\LocationBundle\Model\Location as LocationModel;
use Potato\LocationBundle\Model\Value\Coordinates;
use Potato\LocationBundle\Model\City;
use Potato\LocationBundle\Model\Address as AddressModel;

/**
 * Class Address
 * @package Potato\LocationBundle\Model\Factory\LocationCreationStrategy
 */
class Address implements CreationStrategyInterface
{
    /**
     * @param array $data
     * @param City $city
     * @param Coordinates $coordinates
     * @return Location
     */
    public function create(array $data, City $city, Coordinates $coordinates)
    {
        $location = new LocationModel($city, $coordinates);

        return new AddressModel(
            isset($data['firstLine']) ? $data['firstLine'] : '',
            isset($data['secondLine']) ? $data['secondLine'] : '',
            isset($data['zip']) ? $data['zip'] : '',
            $location
        );
    }
}