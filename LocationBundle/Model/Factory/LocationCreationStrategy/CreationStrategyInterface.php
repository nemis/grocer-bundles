<?php

namespace Potato\LocationBundle\Model\Factory\LocationCreationStrategy;

use Potato\LocationBundle\Model\Value\Coordinates;
use Potato\LocationBundle\Model\City;
use Potato\LocationBundle\Model\Location as LocationModel;

/**
 * Interface CreationStrategyInterface
 * @package Potato\LocationBundle\Model\Factory\LocationCreationStrategy
 */
interface CreationStrategyInterface
{
    /**
     * @param array $data
     * @param City $city
     * @param Coordinates $coordinates
     * @return LocationModel
     */
    public function create(array $data, City $city, Coordinates $coordinates);
}
