<?php

namespace Potato\LocationBundle\Model\Factory\LocationCreationStrategy;

use Potato\LocationBundle\Model\Value\Coordinates;
use Potato\LocationBundle\Model\City;
use Potato\LocationBundle\Model\Location as LocationModel;

/**
 * Class Location
 * @package Potato\LocationBundle\Model\Factory\LocationCreationStrategy
 */
class Location implements CreationStrategyInterface
{
    /**
     * @param array $data
     * @param City $city
     * @param Coordinates $coordinates
     * @return Location
     */
    public function create(array $data, City $city, Coordinates $coordinates)
    {
        return new LocationModel($city, $coordinates);
    }
}
