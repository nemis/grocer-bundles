<?php

namespace Potato\LocationBundle\Model\Access;

use Potato\LocationBundle\Model\Location;

interface LocalizableInterface
{
    /**
     * @param Location $location
     */
    public function setLocation(Location $location);

    /**
     * @return Location
     */
    public function getLocation();
}