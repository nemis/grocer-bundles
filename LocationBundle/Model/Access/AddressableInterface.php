<?php

namespace Potato\LocationBundle\Model\Access;

use Potato\LocationBundle\Model\Address;

interface AddressableInterface
{
    /**
     * @param Address $address
     * @return mixed
     */
    public function setAddress(Address $address);

    /**
     * @return Address
     */
    public function getAddress();
}