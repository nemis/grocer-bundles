<?php

namespace Potato\LocationBundle\Model;

class City
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var State
     */
    protected $state;

    /**
     * @param \Potato\LocationBundle\Model\State $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return \Potato\LocationBundle\Model\State
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $name
     * @param State $state
     */
    public function __construct($name = '', State $state = null)
    {
        $this->name = $name;

        if ($state == null) {
            $country = new Country();
            $state = new State('', $country);
        }

        $this->state = $state;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}