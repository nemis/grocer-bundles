<?php

namespace Potato\LocationBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FindControllerTest extends WebTestCase
{
    /**
     * Test that controller will return latitude and longitude for Paris
     */
    public function testFindAction()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            static::$kernel
                ->getContainer()
                ->get('router')
                ->generate('_location_find', array('location' => 'Paris'))
        );

        $result = json_decode($client->getResponse()->getContent(), true);

        $firstResult = array_shift($result);

        $this->assertEquals($firstResult['coordinates']['latitude'], '48.856614');
        $this->assertEquals($firstResult['coordinates']['longitude'], '2.3522219');
    }

    /**
     * Test that controller will return Paris for latitude and longitude 48.856614 2.3522219
     */
    public function testFindByLatLangAction()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            static::$kernel
                ->getContainer()
                ->get('router')
                ->generate(
                    '_location_find_lat_lang',
                    array('location' => '48.856614, 2.3522219')
                )
        );

        $result = json_decode($client->getResponse()->getContent(), true);

        $firstResult = array_shift($result);

        $this->assertEquals($firstResult['city']['name'], 'Paris');
    }
}