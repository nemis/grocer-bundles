<?php

namespace Potato\LocationBundle\Tests\Validator\Constraints;

use Potato\LocationBundle\Validator\Constraints\ValidAddress;

class ValidAddressTest extends \PHPUnit_Framework_TestCase
{
    public function testHasValidator()
    {
        $validLocation = new ValidAddress();

        $validatedBy = $validLocation->validatedBy();

        $this->assertNotEmpty($validatedBy);
    }
}