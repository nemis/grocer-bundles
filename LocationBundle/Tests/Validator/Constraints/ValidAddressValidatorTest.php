<?php

namespace Potato\LocationBundle\Tests\Validator\Constraints;

use Potato\LocationBundle\Validator\Constraints\ValidAddress;
use Potato\LocationBundle\Validator\Constraints\ValidAddressValidator;

class ValidAddressValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function testDoesntValidateNotFullyDefined()
    {
        $location = $this->getMock('Potato\LocationBundle\Model\Location');

        $address = $this->getMock('Potato\LocationBundle\Model\Address');
        $address->expects($this->once())
            ->method('getLocation')
            ->will($this->returnValue($location));

        $context = $this->getMock('Symfony\Component\Validator\ExecutionContextInterface');

        $context->expects($this->once())
            ->method('addViolation');

        $finder = $this->getMockBuilder('Potato\LocationBundle\Services\Finder')
            ->disableOriginalConstructor()
            ->getMock();

        $finder->expects($this->once())
            ->method('translateString')
            ->will($this->returnValue(array()));

        $validLocationValidator = new ValidAddressValidator($finder);
        $validLocationValidator->initialize($context);

        $constraint = new ValidAddress();

        $validLocationValidator->validate($address, $constraint);
    }
}