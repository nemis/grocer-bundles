<?php

namespace Potato\LocationBundle\Tests\Validator\Constraints;

use Potato\LocationBundle\Validator\Constraints\ValidLocation;

class ValidLocationTest extends \PHPUnit_Framework_TestCase
{
    public function testHasValidator()
    {
        $validLocation = new ValidLocation();

        $validatedBy = $validLocation->validatedBy();

        $this->assertNotEmpty($validatedBy);
    }
}