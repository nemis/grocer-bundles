<?php

namespace Potato\LocationBundle\Tests\Validator\Constraints;

use Potato\LocationBundle\Validator\Constraints\ValidLocation;
use Potato\LocationBundle\Validator\Constraints\ValidLocationValidator;

class ValidLocationValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function testDoesntValidateNotFullyDefined()
    {
        $location = $this->getMock('Potato\LocationBundle\Model\Location');

        $context = $this->getMock('Symfony\Component\Validator\ExecutionContextInterface');

        $context->expects($this->once())
            ->method('addViolation');

        $finder = $this->getMockBuilder('Potato\LocationBundle\Services\Finder')
            ->disableOriginalConstructor()
            ->getMock();

        $validLocationValidator = new ValidLocationValidator($finder);
        $validLocationValidator->initialize($context);

        $constraint = new ValidLocation();

        $validLocationValidator->validate($location, $constraint);
    }
}
