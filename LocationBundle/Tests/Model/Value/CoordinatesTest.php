<?php

namespace Potato\LocationBundle\Tests\Model\Value;

use Potato\LocationBundle\Model\Value\Coordinates;

class CoordinatesTest extends \PHPUnit_Framework_TestCase
{
    public function testProperties()
    {
        $coordinates = new Coordinates(101, 102);

        $this->assertEquals(101, $coordinates->getLatitude());
        $this->assertEquals(102, $coordinates->getLongitude());
    }
}