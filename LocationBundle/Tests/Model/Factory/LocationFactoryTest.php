<?php

namespace Potato\LocationBundle\Tests\Model\Factory\LocationFactory;

use Potato\LocationBundle\Model\Factory\LocationFactory;

/**
 * Class LocationFactoryTest
 * @package Potato\LocationBundle\Tests\Model\Factory\Location
 */
class LocationFactoryTest extends \PHPUnit_Framework_TestCase
{
    private function getStrategyMock()
    {
        $locationModel = $this->getMock('Potato\LocationBundle\Model\Location');

        $cityModel = $this->getMock('Potato\LocationBundle\Model\City');
        $stateModel = $this->getMock('Potato\LocationBundle\Model\State');
        $countryModel = $this->getMock('Potato\LocaitonBundle\Model\Country');
        $coordinatesValue = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $locationModel->expects($this->any())
            ->method('getCity')
            ->will($this->returnValue($cityModel));

        $stateModel->expects($this->any())
            ->method('getCountry')
            ->will($this->returnValue($countryModel));

        $locationModel->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinatesValue));

        $cityModel->expects($this->any())
            ->method('getState')
            ->will($this->returnValue($stateModel));

        $strategy = $this->getMock(
            'Potato\LocationBundle\Model\Factory\LocationCreationStrategy\CreationStrategyInterface'
        );

        $strategy->expects($this->any())
            ->method('create')
            ->will($this->returnValue($locationModel));

        return $strategy;
    }

    /**
     * Test for valid location object from array
     */
    public function testIfReturnsLocationObjectFromArray()
    {
        $factory = new LocationFactory($this->getStrategyMock());

        $location = $factory->createFromArray(
            array(
                'latitude' => 10,
                'longitude' => 20,
                'city' => 'New York',
                'country' => 'USA',
                'state' => 'New York',
                'countryShortName' => 'USA',
                'stateShortName' => 'NY'
            )
        );

        $this->assertInstanceOf('Potato\LocationBundle\Model\Location', $location);
    }

    /**
     * Test for valid location object from nested array
     */
    public function testIfReturnsLocationObjectFromNestedArray()
    {
        $factory = new LocationFactory($this->getStrategyMock());

        $location = $factory->createFromNestedArray(
            array(
                'coordinates' => array(
                    'latitude' => 10,
                    'longitude' => 20
                ),
                'city' => array(
                    'name' => 'New York',
                    'state' => array(
                        'name' => 'New York',
                        'shortName' => 'NY',
                        'country' => array(
                            'name' => 'USA',
                            'shortName' => 'USA'
                        )
                    )
                )
            )
        );

        $this->assertInstanceOf('Potato\LocationBundle\Model\Location', $location);
    }

    /**
    * Test for valid location object from nested array
    */
    public function testIfReturnsLocationObjectFromJson()
    {
        $factory = new LocationFactory($this->getStrategyMock());

        $location = $factory->createFromNestedArray(
            array(
                'coordinates' => array(
                    'latitude' => 10,
                    'longitude' => 20
                ),
                'city' => array(
                    'name' => 'New York',
                    'state' => array(
                        'name' => 'New York',
                        'shortName' => 'NY',
                        'country' => array(
                            'name' => 'USA',
                            'shortName' => 'USA'
                        )
                    )
                )
            )
        );

        $this->assertInstanceOf('Potato\LocationBundle\Model\Location', $location);
    }

    /**
     * Test for throwing exception when the array of data is not fully populated
     */
    public function testShouldThrowExceptionWhenGivenInvalidData()
    {
        $factory = new LocationFactory($this->getStrategyMock());

        $this->setExpectedException('\OutOfBoundsException');

        $factory->createFromArray(array('city' => 'New York'));
    }
}
