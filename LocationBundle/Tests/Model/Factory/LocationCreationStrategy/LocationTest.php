<?php

namespace Potato\LocationBundle\Tests\Model\Factory\LocationCreationStrategy;

use Potato\LocationBundle\Model\Factory\LocationCreationStrategy\Location;

class LocationTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatesLocation()
    {
        $city = $this->getMock('Potato\LocationBundle\Model\City');
        $coordinates = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $data = array();

        $stategy = new Location();

        $this->assertInstanceOf('Potato\LocationBundle\Model\Location', $stategy->create($data, $city, $coordinates));
    }
}