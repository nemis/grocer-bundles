<?php

namespace Potato\LocationBundle\Tests\Model\Factory\LocationCreationStrategy;

use Potato\LocationBundle\Model\Factory\LocationCreationStrategy\Address;

class AddressTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatesLocation()
    {
        $city = $this->getMock('Potato\LocationBundle\Model\City');
        $coordinates = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $data = array();

        $stategy = new Address();

        $this->assertInstanceOf('Potato\LocationBundle\Model\Address', $stategy->create($data, $city, $coordinates));
    }
}