<?php

namespace Potato\LocationBundle\Tests\Adapter;

use Potato\LocationBundle\Adapter\AddressEntity;

class AddressEntityTest extends \PHPUnit_Framework_TestCase
{
    public function testCreationOfAdapter()
    {
        $locationPersistence = $this->getMock(
            'Potato\LocationBundle\Services\Persistence\LocationPersistenceInterface'
        );

        $locationPersistence->expects($this->any())
            ->method('persistLocation')
            ->will($this->returnValue(null));

        $locationModel = $this->getMock('Potato\LocationBundle\Model\Location');

        $addressModel = $this->getMock('Potato\LocationBundle\Model\Address');

        $cityModel = $this->getMock('Potato\LocationBundle\Model\City');
        $stateModel = $this->getMock('Potato\LocationBundle\Model\State');
        $countryModel = $this->getMock('Potato\LocaitonBundle\Model\Country');
        $coordinatesValue = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $locationModel->expects($this->any())
            ->method('getCity')
            ->will($this->returnValue($cityModel));

        $stateModel->expects($this->any())
            ->method('getCountry')
            ->will($this->returnValue($countryModel));

        $locationModel->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinatesValue));

        $cityModel->expects($this->any())
            ->method('getState')
            ->will($this->returnValue($stateModel));

        $addressModel->expects($this->any())
            ->method('getLocation')
            ->will($this->returnValue($locationModel));

        $addressEntity = new AddressEntity($locationPersistence, $addressModel);

        $this->assertEquals($cityModel, $addressEntity->getLocation()->getCity());
        $this->assertEquals($countryModel, $addressEntity->getLocation()->getCity()->getState()->getCountry());
        $this->assertEquals($stateModel, $addressEntity->getLocation()->getCity()->getState());

    }
}