<?php

namespace Potato\LocationBundle\Tests\Adapter;

use Potato\LocationBundle\Adapter\LocationEntity;

class LocationEntityTest extends \PHPUnit_Framework_TestCase
{
    public function testCreationOfAdapter()
    {
        $locationPersistence = $this->getMock(
            'Potato\LocationBundle\Services\Persistence\LocationPersistenceInterface'
        );

        $locationModel = $this->getMock('Potato\LocationBundle\Model\Location');

        $cityModel = $this->getMock('Potato\LocationBundle\Model\City');
        $stateModel = $this->getMock('Potato\LocationBundle\Model\State');
        $countryModel = $this->getMock('Potato\LocaitonBundle\Model\Country');
        $coordinatesValue = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $locationModel->expects($this->any())
            ->method('getCity')
            ->will($this->returnValue($cityModel));

        $stateModel->expects($this->any())
            ->method('getCountry')
            ->will($this->returnValue($countryModel));

        $locationModel->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinatesValue));

        $cityModel->expects($this->any())
            ->method('getState')
            ->will($this->returnValue($stateModel));

        $locationEntity = new LocationEntity($locationPersistence, $locationModel);

        $this->assertEquals($cityModel, $locationEntity->getCity());
        $this->assertEquals($countryModel, $locationEntity->getCity()->getState()->getCountry());
        $this->assertEquals($stateModel, $locationEntity->getCity()->getState());
        $this->assertEquals($coordinatesValue, $locationEntity->getCoordinates());
    }
}
