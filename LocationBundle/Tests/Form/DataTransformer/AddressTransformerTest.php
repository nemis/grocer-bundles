<?php

namespace Potato\LocationBundle\Tests\Form\DataTransformer;

use Potato\LocationBundle\Form\DataTransformer\AddressTransformer;

class AddressTransformerTest extends \PHPUnit_Framework_TestCase
{
    private function getLocationFactoryMock()
    {
        $locationFactory = $this->getMockBuilder('Potato\LocationBundle\Model\Factory\LocationFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $locationModel = $this->getMock('Potato\LocationBundle\Model\Location');

        $cityModel = $this->getMock('Potato\LocationBundle\Model\City');
        $stateModel = $this->getMock('Potato\LocationBundle\Model\State');
        $countryModel = $this->getMock('Potato\LocationBundle\Model\Country');
        $coordinatesValue = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $locationModel->expects($this->any())
            ->method('getCity')
            ->will($this->returnValue($cityModel));

        $locationModel->expects($this->any())
            ->method('getCountry')
            ->will($this->returnValue($countryModel));

        $locationModel->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinatesValue));

        $locationModel->expects($this->any())
            ->method('getState')
            ->will($this->returnValue($stateModel));

        $addressModel = $this->getMock('Potato\LocationBundle\Model\Address');
        $addressModel->expects($this->any())
            ->method('getLocation')
            ->will($this->returnValue($locationModel));

        $locationFactory->expects($this->any())
            ->method('createFromArray')
            ->will($this->returnValue($addressModel));

        $cityModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $stateModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $countryModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        return $locationFactory;
    }

    public function testConvertsToArray()
    {
        $finder = $this->getMockBuilder('Potato\LocationBundle\Services\Finder')
            ->disableOriginalConstructor()
            ->getMock();

        $finder->expects($this->any())
            ->method('revise');

        $location = $this->getMock('Potato\LocationBundle\Model\Location');
        $city = $this->getMock('Potato\LocationBundle\Model\City');
        $country = $this->getMock('Potato\LocationBundle\Model\Country');
        $state = $this->getMock('Potato\LocationBundle\Model\State');
        $coordinates = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $city
            ->expects($this->any())
            ->method('getState')
            ->will($this->returnValue($state));

        $city
            ->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('New York'));

        $state
            ->expects($this->any())
            ->method('getCountry')
            ->will($this->returnValue($country));

        $state
            ->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('New York'));

        $state
            ->expects($this->any())
            ->method('getShortName')
            ->will($this->returnValue('NY'));

        $country
            ->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('United States'));

        $country
            ->expects($this->any())
            ->method('getShortName')
            ->will($this->returnValue('US'));

        $address = $this->getMock('Potato\LocationBundle\Model\Address');

        $location
            ->expects($this->any())
            ->method('getCity')
            ->will($this->returnValue($city));

        $location
            ->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinates));

        $coordinates
            ->expects($this->any())
            ->method('getLatitude')
            ->will($this->returnValue('10'));

        $coordinates
            ->expects($this->any())
            ->method('getLongitude')
            ->will($this->returnValue('10'));

        $address
            ->expects($this->any())
            ->method('getLocation')
            ->will($this->returnValue($location));

        $address
            ->expects($this->any())
            ->method('getFirstLine')
            ->will($this->returnValue('Brooklyn 1'));

        $address
            ->expects($this->any())
            ->method('getSecondLine')
            ->will($this->returnValue('123'));

        $address
            ->expects($this->any())
            ->method('getZip')
            ->will($this->returnValue('555'));

        $addressTransformer = new AddressTransformer(
            $this->getLocationFactoryMock(),
            $finder
        );

        $result = $addressTransformer->transform($address);

        $this->assertEquals('New York', $result['city']);
        $this->assertEquals('New York', $result['state']);
        $this->assertEquals('NY', $result['stateShortName']);
        $this->assertEquals('United States', $result['country']);
        $this->assertEquals('US', $result['countryShortName']);
        $this->assertEquals('Brooklyn 1', $result['firstLine']);
        $this->assertEquals('123', $result['secondLine']);
        $this->assertEquals('555', $result['zip']);
    }

    public function testReverseTransform()
    {
        $data = array(
            'city' => 'New York',
            'state' => 'New York',
            'stateShortName' => 'New York',
            'country' => 'United States',
            'countryShortName' => 'US',
            'firstLine' => 'test',
            'secondLine' => 'test',
            'zip' => 'test'
        );

        $finder = $this->getMockBuilder('Potato\LocationBundle\Services\Finder')
            ->disableOriginalConstructor()
            ->getMock();

        $finder->expects($this->any())
            ->method('revise');

        $addressTransformer = new AddressTransformer(
            $this->getLocationFactoryMock(),
            $finder
        );

        $result = $addressTransformer->reverseTransform($data);

        $this->assertInstanceOf('Potato\LocationBundle\Model\Address', $result);
    }
}