<?php

namespace Potato\LocationBundle\Tests\Form\DataTransformer;

use Potato\LocationBundle\Form\DataTransformer\LocationTransformer;
use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Model\Value\Coordinates;
use Potato\LocationBundle\Model\City;
use Potato\LocationBundle\Model\Country;
use Potato\LocationBundle\Model\State;

class LocationTransformerTest extends \PHPUnit_Framework_TestCase
{
    private function getLocationFactoryMock()
    {
        $locationFactory = $this->getMockBuilder('Potato\LocationBundle\Model\Factory\LocationFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $locationModel = $this->getMock('Potato\LocationBundle\Model\Location');

        $cityModel = $this->getMock('Potato\LocationBundle\Model\City');
        $stateModel = $this->getMock('Potato\LocationBundle\Model\State');
        $countryModel = $this->getMock('Potato\LocationBundle\Model\Country');
        $coordinatesValue = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $locationModel->expects($this->any())
            ->method('getCity')
            ->will($this->returnValue($cityModel));

        $locationModel->expects($this->any())
            ->method('getCountry')
            ->will($this->returnValue($countryModel));

        $locationModel->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinatesValue));

        $locationModel->expects($this->any())
            ->method('getState')
            ->will($this->returnValue($stateModel));

        $locationFactory->expects($this->any())
            ->method('createFromJson')
            ->will($this->returnValue($locationModel));

        $cityModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $stateModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $countryModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        return $locationFactory;
    }

    public function testTransformsJsonToLocation()
    {
        $finder = $this->getMockBuilder('Potato\LocationBundle\Services\Finder')
            ->disableOriginalConstructor()
            ->getMock();

        $finder->expects($this->any())
            ->method('revise');

        $serializer = $this->getMock('Symfony\Component\Serializer\SerializerInterface');

        $locationTransformer = new LocationTransformer($serializer, $finder, $this->getLocationFactoryMock());

        $this->assertEquals('test', $locationTransformer->reverseTransform('{jsonmock}')->getCity()->getName());
    }

    public function testTransformsLocationToJson()
    {
        $locationJson = json_encode(array(
            'city' => 'New York',
            'state' => 'New York',
            'country' => 'USA',
            'longitude' => '10',
            'latitude' => '10'
        ));

        $factory = $this->getLocationFactoryMock();
        $serializer = $this->getMock('Symfony\Component\Serializer\SerializerInterface');
        $serializer->expects($this->any())
            ->method('serialize')
            ->will($this->returnValue($locationJson));

        $location = new Location();
        $location->setCity(new City('New York'));
        $location->getCity()->setState(new State('New York'));
        $location->getCity()->getState()->setCountry(new Country('USA'));
        $location->setCoordinates(new Coordinates(10, 10));

        $finder = $this->getMockBuilder('Potato\LocationBundle\Services\Finder')
            ->disableOriginalConstructor()
            ->getMock();

        $finder->expects($this->any())
            ->method('revise');

        $locationTransformer = new LocationTransformer($serializer, $finder, $factory);

        $this->assertEquals(
            json_decode($locationJson, true),
            json_decode($locationTransformer->transform($location), true)
        );
    }
}