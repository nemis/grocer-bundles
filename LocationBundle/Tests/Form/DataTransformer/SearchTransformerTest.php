<?php

namespace Potato\LocationBundle\Tests\Form\DataTransformer;

use Potato\LocationBundle\Form\DataTransformer\SearchTransformer;

/**
 * Class SearchTransformerTest
 * @package Potato\LocationBundle\Tests\Form\DataTransformer
 */
class SearchTransformerTest extends \PHPUnit_Framework_TestCase
{
    public function testTransformsTwoWays()
    {
        $string = 'New York';

        $location = $this->getMock('Potato\LocationBundle\Model\Location');

        $finder = $this->getMockBuilder('Potato\LocationBundle\Services\Finder')
            ->disableOriginalConstructor()
            ->getMock();

        $finder->expects($this->atLeastOnce())
            ->method('translateString')
            ->will($this->returnValue([$location]));

        $coordinates = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $coordinates->expects($this->atLeastOnce())
            ->method('getLatitude')
            ->will($this->returnValue(10));

        $coordinates->expects($this->atLeastOnce())
            ->method('getLongitude')
            ->will($this->returnValue(10));

        $location->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinates));

        $transformer = new SearchTransformer($finder);

        $transformed = $transformer->reverseTransform($string);

        $this->assertEquals('10,10', $transformer->transform($transformed));
    }
}