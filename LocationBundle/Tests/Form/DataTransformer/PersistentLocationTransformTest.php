<?php

namespace Potato\LocationBundle\Tests\Form\DataTransformer;

use Potato\LocationBundle\Form\DataTransformer\PersistentLocationTransformer;

class PersistentLocationTransformerTest extends \PHPUnit_Framework_TestCase
{
    private function getLocationFactoryMock()
    {
        $locationFactory = $this->getMockBuilder('Potato\LocationBundle\Model\Factory\LocationFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $locationModel = $this->getMock('Potato\LocationBundle\Model\Location');

        $cityModel = $this->getMock('Potato\LocationBundle\Model\City');
        $stateModel = $this->getMock('Potato\LocationBundle\Model\State');
        $countryModel = $this->getMock('Potato\LocationBundle\Model\Country');
        $coordinatesValue = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $locationModel->expects($this->any())
            ->method('getCity')
            ->will($this->returnValue($cityModel));

        $locationModel->expects($this->any())
            ->method('getCountry')
            ->will($this->returnValue($countryModel));

        $locationModel->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinatesValue));

        $locationModel->expects($this->any())
            ->method('getState')
            ->will($this->returnValue($stateModel));

        $locationFactory->expects($this->any())
            ->method('createFromJson')
            ->will($this->returnValue($locationModel));

        $cityModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $stateModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $countryModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        return $locationFactory;
    }

    public function testConvertsToLocationEntity()
    {
        $serializer = $this->getMock('Symfony\Component\Serializer\SerializerInterface');
        $locationFactory = $this->getLocationFactoryMock();
        $locationPersistence = $this->getMock(
            'Potato\LocationBundle\Services\Persistence\LocationPersistenceInterface'
        );

        $finder = $this->getMockBuilder('Potato\LocationBundle\Services\Finder')
            ->disableOriginalConstructor()
            ->getMock();

        $finder->expects($this->any())
            ->method('revise');

        $persistentLocationTransformer = new PersistentLocationTransformer(
            $serializer,
            $locationFactory,
            $finder,
            $locationPersistence
        );

        $this->assertInstanceOf(
            'Potato\LocationBundle\Adapter\LocationEntity',
            $persistentLocationTransformer->reverseTransform('["jsonmock"]')
        );
    }
}