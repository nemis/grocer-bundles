<?php

namespace Potato\LocationBundle\Tests\Services;

use Ivory\GoogleMap\Services\Geocoding\Result;
use Potato\LocationBundle\Services\Finder;

class FinderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Potato\LocationBundle\Services\Finder
     */
    private $service;

    /**
     * Prepare service mock
     */
    private function prepareMock()
    {
        $provider = $this->getMock('Geocoder\Provider\ProviderInterface');

        $locationData = $this->getMock('stdClass', array('getResults'));

        $latLang = $this->getMock('stdClass', array('getLatitude', 'getLongitude'));
        $latLang->expects($this->any())
            ->method('getLatitude')
            ->will($this->returnValue('10'));

        $latLang->expects($this->any())
            ->method('getLongitude')
            ->will($this->returnValue('10'));

        $geometry = $this->getMock('stdClass', array('getLocation'));
        $geometry->expects($this->any())
            ->method('getLocation')
            ->will($this->returnValue($latLang));

        $locationDataResults = $this->getMock('stdClass', array('getGeometry', 'getAddressComponents'));
        $locationDataResults->expects($this->any())
            ->method('getGeometry')
            ->will($this->returnValue($geometry));

        $locationData->expects($this->any())
            ->method('getResults')
            ->will($this->returnValue([$locationDataResults]));

        $provider->expects($this->any())
            ->method('getGeocodedData')
            ->will($this->returnValue($locationData));

        $addressComponents = array();
        $addressComponents[0] = $this->getMock('stdClass', array('getTypes', 'getLongName', 'getShortName'));
        $addressComponents[0]->expects($this->any())
            ->method('getTypes')
            ->will($this->returnValue(array('location')));
        $addressComponents[0]->expects($this->any())
            ->method('getLongName')
            ->will($this->returnValue('Testing location'));
        $addressComponents[0]->expects($this->any())
            ->method('getShortName')
            ->will($this->returnValue('Testing location'));

        $addressComponents[1] = $this->getMock('stdClass', array('getTypes', 'getLongName', 'getShortName'));
        $addressComponents[1]->expects($this->any())
            ->method('getTypes')
            ->will($this->returnValue(array('administrative_area_level_1')));
        $addressComponents[1]->expects($this->any())
            ->method('getLongName')
            ->will($this->returnValue('Testing administrative'));
        $addressComponents[1]->expects($this->any())
            ->method('getShortName')
            ->will($this->returnValue('Testing administrative'));

        $addressComponents[2] = $this->getMock('stdClass', array('getTypes', 'getLongName', 'getShortName'));
        $addressComponents[2]->expects($this->any())
            ->method('getTypes')
            ->will($this->returnValue(array('country')));
        $addressComponents[2]->expects($this->any())
            ->method('getLongName')
            ->will($this->returnValue('Testing country'));
        $addressComponents[2]->expects($this->any())
            ->method('getShortName')
            ->will($this->returnValue('Testing country'));

        $locationDataResults->expects($this->any())
            ->method('getAddressComponents')
            ->will($this->returnValue($addressComponents));

        $locationFactory = $this->getMockBuilder('Potato\LocationBundle\Model\Factory\LocationFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $locationModel = $this->getMock('Potato\LocationBundle\Model\Location');

        $locationModel->expects($this->atLeastOnce())
            ->method('isFullyDefined')
            ->will($this->returnValue(true));

        $cityModel = $this->getMock('Potato\LocationBundle\Model\City');
        $stateModel = $this->getMock('Potato\LocationBundle\Model\State');
        $countryModel = $this->getMock('Potato\LocationBundle\Model\Country');
        $coordinatesValue = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $locationModel->expects($this->any())
            ->method('getCity')
            ->will($this->returnValue($cityModel));

        $stateModel->expects($this->any())
            ->method('getCountry')
            ->will($this->returnValue($countryModel));

        $locationModel->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinatesValue));

        $cityModel->expects($this->any())
            ->method('getState')
            ->will($this->returnValue($stateModel));

        $locationFactory->expects($this->any())
            ->method('createFromArray')
            ->will($this->returnValue($locationModel));

        $cityModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $stateModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $countryModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $this->service = new Finder($locationFactory, $provider);
    }

    public function testServiceIsTranslatingString()
    {
        $this->prepareMock();

        $results = $this->service->translateString('test');

        $this->assertGreaterThan(0, count($results));

        $result = array_shift($results);

        $this->assertEquals($result->getCity()->getName(), 'test');
        $this->assertEquals($result->getCity()->getState()->getCountry()->getName(), 'test');
        $this->assertEquals($result->getCity()->getState()->getName(), 'test');
    }

    public function testServiceIsTranslatingCords()
    {
        $this->prepareMock();

        $results = $this->service->translateLatLang(10, 10);

        $this->assertGreaterThan(0, count($results));

        $result = array_shift($results);
        $this->assertEquals($result->getCity()->getName(), 'test');
        $this->assertEquals($result->getCity()->getState()->getCountry()->getName(), 'test');
        $this->assertEquals($result->getCity()->getState()->getName(), 'test');
    }

    public function testRevisingLocation()
    {
        $this->prepareMock();

        $locationModel = $this->getMock('Potato\LocationBundle\Model\Location');

        $cityModel = $this->getMock('Potato\LocationBundle\Model\City');
        $stateModel = $this->getMock('Potato\LocationBundle\Model\State');
        $countryModel = $this->getMock('Potato\LocationBundle\Model\Country');
        $coordinatesValue = $this->getMock('Potato\LocationBundle\Model\Value\Coordinates');

        $locationModel->expects($this->any())
            ->method('getCity')
            ->will($this->returnValue($cityModel));

        $stateModel->expects($this->any())
            ->method('getCountry')
            ->will($this->returnValue($countryModel));

        $locationModel->expects($this->any())
            ->method('getCoordinates')
            ->will($this->returnValue($coordinatesValue));

        $cityModel->expects($this->any())
            ->method('getState')
            ->will($this->returnValue($stateModel));

        $cityModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $stateModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $countryModel->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));

        $locationModel->expects($this->atLeastOnce())
            ->method('setCity');

        $this->service->reviseLocation($locationModel);
    }
}