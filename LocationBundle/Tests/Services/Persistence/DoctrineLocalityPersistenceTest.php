<?php

namespace Potato\LocationBundle\Tests\Services\Persistence;

use \Mockery as m;
use Doctrine\ORM\UnitOfWork;
use Potato\LocationBundle\Services\Persistence\DoctrineLocationPersistence;

class DoctrineLocationPersistenceTest extends \PHPUnit_Framework_TestCase
{
    private function getDoctrineMock($queryBuilder = null)
    {
        $doctrineMock = m::mock('Doctrine\ORM\EntityManager', array(
            'flush' => null,
            'persist' => null
        ));

        $doctrineMock
            ->shouldReceive('getUnitOfWork->getEntityState')
            ->andReturn(UnitOfWork::STATE_MANAGED);

        if ($queryBuilder) {
            $doctrineMock
                ->shouldReceive('createQueryBuilder')
                ->andReturn($queryBuilder);
        }

        return $doctrineMock;
    }

    private function getLocationMock()
    {
        return m::mock('Potato\LocationBundle\Model\Location', array(
            'getCity' => m::mock('Potato\LocationBundle\Model\City', array(
                'getName' => 'test',
                'getState' => m::mock(
                    'Potato\LocationBundle\Model\State',
                    array(
                        'getName' => 'teststate',
                        'getShortName' => 'testate',
                        'getCountry' => m::mock(
                            'Potato\LocationBundle\Model\Country',
                            array(
                                'getName' => 'testcountry',
                                'getShortName' => 'testcountry'
                            )
                        ),
                        'setCountry' => null
                    )
                ),
                'setState' => null
            )),
            'getCoordinatses' => m::mock('Potato\LocationBundle\Model\Value\Coordinates', array(
                'getLatitude' => 10,
                'getLongitude' => 10
            )),
            'setCity' => null
        ));
    }

    private function getFinderMock()
    {
        $finder = $this->getMockBuilder('Potato\LocationBundle\Services\Finder')
            ->disableOriginalConstructor()
            ->getMock();

        return $finder;
    }

    private function getQueryBuilderMock()
    {
        $queryBuilder = $this->getMock('\StdClass', array(
            'from',
            'where',
            'join',
            'setParameter',
            'getQuery',
            'setMaxResults',
            'getResult',
            'addSelect',
            'andWhere'
        ));

        $methods = array(
            'from' => $queryBuilder,
            'where' => $queryBuilder,
            'join' => $queryBuilder,
            'setParameter' => $queryBuilder,
            'getQuery' => $queryBuilder,
            'setMaxResults' => $queryBuilder,
            'getResult' => array(),
            'addSelect' => $queryBuilder,
            'andWhere' => $queryBuilder
        );

        foreach ($methods as $method => $value) {
            $queryBuilder->expects($this->any())
                ->method($method)
                ->will($this->returnValue($value));
        }

        return $queryBuilder;
    }

    private function getEntityMock($class = 'stdClass')
    {
        return m::mock($class, array(
            'getName' => 'testName',
            'getShortName' => 'testShortName',
            'setName' => null,
            'setShortName' => null
        ));
    }

    public function testPersistingLocation()
    {
        $doctrineMock = $this->getDoctrineMock($this->getQueryBuilderMock());

        $locationMock = $this->getLocationMock();

        $locationMock
            ->shouldReceive('setCity')
            ->atLeast(1);

        $doctrineLocationPersistence = new DoctrineLocationPersistence($doctrineMock, $this->getFinderMock());
        $doctrineLocationPersistence->persistLocation($locationMock);
    }

    public function testConvertingCityToEntity()
    {
        $queryBuilder = m::mock('\StdClass');

        $doctrineMock = $this->getDoctrineMock($this->getQueryBuilderMock());

        $doctrineLocationPersistence = new DoctrineLocationPersistence($doctrineMock, $this->getFinderMock());
        $city = $doctrineLocationPersistence->getCityEntity($this->getLocationMock(), true);

        $this->assertInstanceOf('\Potato\LocationBundle\Entity\City', $city);
    }

    public function testConvertingStateToEntity()
    {
        $queryBuilder = m::mock('\StdClass');

        $doctrineMock = $this->getDoctrineMock($this->getQueryBuilderMock());

        $doctrineLocationPersistence = new DoctrineLocationPersistence($doctrineMock, $this->getFinderMock());
        $state = $doctrineLocationPersistence->getStateEntity($this->getLocationMock(), true);

        $this->assertInstanceOf('\Potato\LocationBundle\Entity\State', $state);
    }

    public function testConvertingCountryToEntity()
    {
        $queryBuilder = m::mock('\StdClass');

        $doctrineMock = $this->getDoctrineMock($this->getQueryBuilderMock());

        $doctrineLocationPersistence = new DoctrineLocationPersistence($doctrineMock, $this->getFinderMock());
        $country = $doctrineLocationPersistence->getCountryEntity($this->getLocationMock(), true);

        $this->assertInstanceOf('\Potato\LocationBundle\Entity\Country', $country);
    }

    public function tearDown()
    {
        m::close();
    }
}