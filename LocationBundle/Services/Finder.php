<?php

namespace Potato\LocationBundle\Services;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Ivory\GoogleMap\Services\Geocoding\GeocoderRequest;
use Geocoder\Provider\ProviderInterface;
use Potato\LocationBundle\Model\CoordinableInterface;
use Potato\LocationBundle\Model\Country;
use Potato\LocationBundle\Model\Factory\LocationFactory;
use Potato\LocationBundle\Model\Location;

/**
 * Service for working with location data,
 * Converts data from geocoder to Location value object.
 *
 * @package Potato\Grocery\UserBundle\Services
 */
class Finder
{
    /**
     * @var \Geocoder\Provider\ProviderInterface
     */
    private $geocoderProvider;

    /**
     * @var string
     */
    private $language = 'pl';

    /**
     * @param LocationFactory $locationFactory
     * @param ProviderInterface $geocoderProvider
     * @param null $language
     */
    public function __construct(
        LocationFactory $locationFactory,
        ProviderInterface $geocoderProvider,
        $language = null
    ) {
        $this->locationFactory = $locationFactory;
        $this->geocoderProvider = $geocoderProvider;
        $this->language = $language;
    }

    /**
     * @param null $value
     * @return GeocoderRequest
     */
    private function createRequest($value = null)
    {
        $geocoderRequest = new GeocoderRequest();
        $geocoderRequest->setAddress($value);

        if ($this->language != null) {
            $geocoderRequest->setLanguage($this->language);
        }

        return $geocoderRequest;
    }

    /**
     * @param string $value
     * @return array
     * @throws \LogicException
     */
    public function translateString($value)
    {
        $request = $this->createRequest($value);

        $locationData = $this->geocoderProvider->getGeocodedData($request);

        return $this->createLocations($locationData->getResults());
    }

    /**
     * Searches for matching location in the remote service
     * and modify the given location values
     * based on found result, to keep the data unique and consistent.
     *
     * @param Location $location
     */
    public function reviseLocation(Location $location)
    {
        $results = $this->translateString(
            $location->getCity()->getName()
            .' city, '
            .$location->getCity()->getState()->getName()
            .', '
            .$location->getCity()->getState()->getCountry()->getName()
        );

        if (empty($results)) {
            return;
        }

        $result = array_shift($results);

        $location->setCity($result->getCity());
        $location->setCoordinates($result->getCoordinates());
    }

    /**
     * Find places by coordinates
     *
     * @param string|double $lat
     * @param string|double $lang
     * @return array
     */
    public function translateLatLang($lat, $lang)
    {
        return $this->findPlaces($lat, $lang);
    }

    /**
     * @param string|double $lat
     * @param string|double $lang
     * @return array
     */
    private function findPlaces($lat, $lang)
    {
        $reverseRequest = $this->createRequest();
        $reverseRequest->setCoordinate($lat, $lang);
        $reverseResult = $this->geocoderProvider->getGeocodedData($reverseRequest);

        return $this->createLocations($reverseResult->getResults());
    }

    /**
     * Creates Location model object from array of results
     * given from external service
     *
     * @param array $results
     * @return array
     */
    private function createLocations($results)
    {
        $resultsArray = [];

        foreach ($results as $result) {
            $locationData = [];

            $location = $result->getGeometry()->getLocation();

            $locationData['latitude'] = $location->getLatitude();
            $locationData['longitude'] = $location->getLongitude();

            $components = $result->getAddressComponents();

            foreach ($components as $component) {
                if (in_array('locality', $component->getTypes())) {
                    $locationData['city'] = $component->getLongName();
                }

                if (in_array('administrative_area_level_1', $component->getTypes())) {
                    $locationData['state'] = $component->getLongName();
                    $locationData['stateShortName'] = $component->getShortName();
                }

                if (in_array('country', $component->getTypes())) {
                    $locationData['country'] = $component->getLongName();
                    $locationData['countryShortName'] = $component->getShortName();
                }
            }

            try {
                $locationValue = $this->locationFactory->createFromArray($locationData);

                if ($locationValue->isFullyDefined()) {
                    $resultsArray[$locationValue->getSumHash()] = $locationValue;
                }
            } catch (\OutOfBoundsException $e) {
                continue;
            }
        }

        return $resultsArray;
    }
}