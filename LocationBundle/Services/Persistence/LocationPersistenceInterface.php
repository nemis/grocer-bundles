<?php

namespace Potato\LocationBundle\Services\Persistence;

use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Entity\City;
use Potato\LocationBundle\Entity\Country;
use Potato\LocationBundle\Entity\State;

interface LocationPersistenceInterface
{
    /**
     * @param Location $location
     */
    public function persistLocation(Location $location);

    /**
     * @param Location $location
     * @return Country
     */
    public function getCountryEntity(Location $location);

    /**
     * @param Location $location
     * @return State
     */
    public function getStateEntity(Location $location);

    /**
     * @param Location $location
     * @return City
     */
    public function getCityEntity(Location $location);
}