<?php

namespace Potato\LocationBundle\Services\Persistence;

use Potato\LocationBundle\Model\Location;
use Potato\LocationBundle\Model\City;
use Potato\LocationBundle\Model\State;
use Potato\LocationBundle\Model\Country;

use Potato\LocationBundle\Entity\Country as CountryEntity;
use Potato\LocationBundle\Entity\State as StateEntity;
use Potato\LocationBundle\Entity\City as CityEntity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\UnitOfWork;
use Potato\LocationBundle\Services\Finder;

/**
 * Class DoctrineLocationPersistence
 *
 * @todo translate names
 *
 * @package Potato\LocationBundle\Services\Persistence
 */
class DoctrineLocationPersistence implements LocationPersistenceInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     * @param Finder $finder
     * @param bool $revise
     */
    public function __construct(EntityManager $entityManager, Finder $finder, $revise = false)
    {
        $this->entityManager = $entityManager;
        $this->finder = $finder;
        $this->revise = $revise;
    }

    /**
     * @param Location $location
     * @param bool $flush
     */
    public function persistLocation(Location $location, $flush = false)
    {
        if ($this->revise) {
            $this->finder->reviseLocation($location);
        }

        if (!$location->getCity()->getState()->getCountry() instanceof CountryEntity) {
            $countryEntity = $this->convertCountryToDoctrineEntity($location->getCity()->getState()->getCountry());
            $this->ensureIsPersistent($countryEntity, $flush);
            $location->getCity()->getState()->setCountry($countryEntity);
        }

        if (!$location->getCity()->getState() instanceof StateEntity) {
            $stateEntity = $this->convertStateToDoctrineEntity($location->getCity()->getState());
            $this->ensureIsPersistent($stateEntity, $flush);
            $location->getCity()->setState($stateEntity);
        }

        if (!$location->getCity() instanceof CityEntity) {
            $cityEntity = $this->convertCityToDoctrineEntity($location->getCity());
            $this->ensureIsPersistent($cityEntity, $flush);
            $location->setCity($cityEntity);
        }
    }

    /**
     * @param mixed $entity
     * @param bool $flush
     */
    private function ensureIsPersistent($entity, $flush = false)
    {
        if (!$this->isPersisted($entity)) {
            $this->entityManager->persist($entity);

            if ($flush) {
                if ($entity instanceof Country) {
                }
                $this->entityManager->flush($entity);
            }
        }
    }

    /**
     * @param mixed $entity
     * @return bool
     */
    private function isPersisted($entity)
    {
        return UnitOfWork::STATE_MANAGED === $this->entityManager->getUnitOfWork()->getEntityState($entity);
    }

    /**
     * @param City $city
     * @return CityEntity
     */
    private function convertCityToDoctrineEntity(City $city)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder
            ->addSelect('c')
            ->from('PotatoLocationBundle:City', 'c')
            ->join('PotatoLocationBundle:State', 's', 'WITH', 'c.state = s')
            ->join('PotatoLocationBundle:Country', 'cr', 'WITH', 's.country = cr')
            ->where('c.name = :name')
            ->andWhere('s.name = :sname')
            ->andWhere('cr.name = :crname')
            ->setParameter('name', $city->getName())
            ->setParameter('sname', $city->getState()->getName())
            ->setParameter('crname', $city->getState()->getCountry()->getName());

        $result = $queryBuilder->getQuery()->setMaxResults(1)->getResult();

        if (empty($result)) {
            $entity = new CityEntity();

            $entity->setName($city->getName());
            $entity->setState($this->convertStateToDoctrineEntity($city->getState()));
            $entity->getState()->setCountry($this->convertCountryToDoctrineEntity($city->getState()->getCountry()));
        } else {
            $entity = array_pop($result);
        }

        return $entity;
    }

    /**
     * @param State $state
     * @return StateEntity
     */
    private function convertStateToDoctrineEntity(State $state)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder
            ->addSelect('s')
            ->from('PotatoLocationBundle:State', 's')
            ->join('PotatoLocationBundle:Country', 'cr', 'WITH', 's.country = cr')
            ->where('s.name = :name')
            ->andWhere('cr.name = :crname')
            ->setParameter('name', $state->getName())
            ->setParameter('crname', $state->getCountry()->getName());

        $result = $queryBuilder->getQuery()->setMaxResults(1)->getResult();

        if (empty($result)) {
            $entity = new StateEntity();

            $entity->setName($state->getName());
            $entity->setShortName($state->getShortName());
            $entity->setCountry($this->convertCountryToDoctrineEntity($state->getCountry()));
        } else {
            $entity = array_pop($result);
        }

        return $entity;
    }

    /**
     * @param Country $country
     * @return CountryEntity
     */
    private function convertCountryToDoctrineEntity(Country $country)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder
            ->addSelect('cr')
            ->from('PotatoLocationBundle:Country', 'cr')
            ->where('cr.name = :name')
            ->setParameter('name', $country->getName());

        $result = $queryBuilder->getQuery()->setMaxResults(1)->getResult();

        if (empty($result)) {
            $entity = new CountryEntity();
            $entity->setName($country->getName());
            $entity->setShortName($country->getShortName());
        } else {
            $entity = array_pop($result);
        }

        return $entity;
    }

    /**
     * @param Location $location
     * @return CityEntity
     */
    public function getCityEntity(Location $location)
    {
        $entity = $this->convertCityToDoctrineEntity($location->getCity());

        return $entity;
    }

    /**
     * @param Location $location
     * @return CountryEntity
     */
    public function getCountryEntity(Location $location)
    {
        $entity = $this->convertCountryToDoctrineEntity($location->getCity()->getState()->getCountry());

        return $entity;
    }

    /**
     * @param Location $location
     * @return StateEntity
     */
    public function getStateEntity(Location $location)
    {
        $entity = $this->convertStateToDoctrineEntity($location->getCity()->getState());

        return $entity;
    }
}