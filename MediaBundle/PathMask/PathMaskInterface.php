<?php

namespace Potato\MediaBundle\PathMask;

use Potato\MediaBundle\Accessor\Location\FilePath;

interface PathMaskInterface
{
    /**
     * @param $string
     * @return string
     */
    public function maskString($string);

    /**
     * @param $string
     * @return string
     */
    public function unmaskString($string);

    /**
     * @param FilePath $filePath
     * @return FilePath
     */
    public function maskFilePath(FilePath $filePath);

    /**
     * @param FilePath $filePath
     * @return FilePath
     */
    public function unmaskFilePath(FilePath $filePath);
}