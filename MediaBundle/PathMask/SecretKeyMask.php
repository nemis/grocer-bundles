<?php

namespace Potato\MediaBundle\PathMask;

use Potato\MediaBundle\Accessor\Location\FilePath;

class SecretKeyMask implements PathMaskInterface
{
    /**
     * @var string
     */
    private $secretKey;

    const METHOD = "AES-256-CBC";

    /**
     * @param string $secretKey
     * @param string $iv
     */
    public function __construct($secretKey, $iv)
    {
        $this->secretKey = $secretKey;
        $this->iv = $iv;
    }

    /**
     * @param string $string
     * @return string
     */
    private function encrypt($string)
    {
        $string = openssl_encrypt($string, self::METHOD, $this->secretKey, false, $this->iv);
        $len = strlen($string);

        $result = '';

        for ($i=0; $i<$len; $i++) {
            $result .= str_pad(ord($string{$i}), 3, '0', STR_PAD_LEFT);
        }

        return $result;
    }

    /**
     * @param string $string
     * @return string
     */
    private function decrypt($string)
    {
        $len = strlen($string);

        $decryptString = '';

        for ($i=0; $i<$len; $i+=3) {
            if (isset($string{$i+2})) {
                $char = $string{$i}.$string{$i+1}.$string{$i+2};
                $decryptString .= chr(intval($char));
            }
        }

        return openssl_decrypt($decryptString, self::METHOD, $this->secretKey, false, $this->iv);
    }

    /**
     * @param $string
     * @return string
     */
    public function maskString($string)
    {
        return $this->encrypt($string);
    }

    /**
     * @param $string
     * @return string
     */
    public function unmaskString($string)
    {
        return $this->decrypt($string);
    }

    /**
     * @param FilePath $filePath
     * @return FilePath
     */
    public function maskFilePath(FilePath $filePath)
    {
        $file = $filePath->getFile();
        $path = $filePath->getPath();

        $fileString = $this->encrypt($file->asString());
        $pathString = $this->encrypt($filePath->asString());

        $path = $path->parseString($pathString);
        $file->setName($fileString);

        $filePath->setPath($path);
        $filePath->setFile($file);

        return $filePath;
    }

    /**
     * @param FilePath $filePath
     * @return FilePath
     */
    public function unmaskFilePath(FilePath $filePath)
    {
        $file = $filePath->getFile();
        $path = $filePath->getPath();

        $fileString = $this->decrypt($file->asString());
        $pathString = $this->decrypt($filePath->asString());

        $path = $path->parseString($pathString);
        $file->setName($fileString);

        $filePath->setPath($path);
        $filePath->setFile($file);

        return $filePath;
    }
}