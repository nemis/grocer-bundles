<?php

namespace Potato\MediaBundle\PathTranslation;

use Potato\MediaBundle\Accessor\Location\FilePath;
use Potato\MediaBundle\Accessor\Location\FilePathFactory;

/**
 * Class Translator
 * @package Potato\MediaBundle\PathTranslation
 */
abstract class AbstractTranslator
{
    /**
     * @var string
     */
    protected $baseDir;

    /**
     * @var \Potato\MediaBundle\Accessor\Location\FilePathFactory
     */
    protected $filePathFactory;

    /**
     * @param string $baseDir
     * @param FilePathFactory $filePathFactory
     */
    public function __construct($baseDir, FilePathFactory $filePathFactory)
    {
        $this->baseDir = $baseDir;
        $this->filePathFactory = $filePathFactory;
    }

    /**
     * @param mixed $object
     * @param string $propertyName
     * @param FilePath $path
     * @param string|null $desiredExtension
     * @return mixed
     */
    abstract public function translate($object, $propertyName, FilePath $path, $desiredExtension = null);
}