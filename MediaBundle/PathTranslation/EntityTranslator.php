<?php

namespace Potato\MediaBundle\PathTranslation;

use Potato\MediaBundle\Accessor\Location\FilePath;

class EntityTranslator extends AbstractTranslator
{
    /**
     * {@inheritDoc}
     */
    public function translate($object, $propertyName, FilePath $path, $desiredExtension = null)
    {
        if (!($id = @$object->getId())) {
            $id = uniqid('uploaded');
        }

        $entityName = get_class($object);

        $explodedName = explode('\\', $entityName);

        if (count($explodedName) > 0) {
            $entityName = array_pop($explodedName);
        }

        if (is_null($desiredExtension)) {
            $desiredExtension = $path->getFile()->getExtension();
        }

        $filePath = $this->filePathFactory->createFromString(
            $this->baseDir
            .'/'
            .strtolower($entityName)
            .'/'
            .$id
            .'_'
            .$propertyName
            .'.'
            .$desiredExtension
        );

        $filePath->getFile()->setContents($path->getFile()->getContents());

        return $filePath;
    }
}