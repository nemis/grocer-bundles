<?php

namespace Potato\MediaBundle\Routing;

interface RouterInterface
{
    /**
     * @param string $path
     * @param array $options
     * @return string
     */
    public function get($path, array $options = array());
}