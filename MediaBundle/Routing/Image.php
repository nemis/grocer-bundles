<?php

namespace Potato\MediaBundle\Routing;

use Potato\MediaBundle\Accessor\Location\FilePath;
use Potato\MediaBundle\PathMask\PathMaskInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Image implements RouterInterface
{
    /**
     * @var \Potato\MediaBundle\PathMask\PathMaskInterface
     */
    private $pathMask;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    private $router;

    /**
     * @param PathMaskInterface $pathMask
     * @param Router $router
     */
    public function __construct(PathMaskInterface $pathMask, Router $router)
    {
        $this->pathMask = $pathMask;
        $this->router = $router;
    }

    /**
     * @param mixed $path
     * @param array $options
     * @return string
     */
    public function get($path, array $options = array())
    {
        if ($path instanceof FilePath) {
            $path = $path->asString();
        }

        if (isset($options['encodePath']) && (bool)$options['encodePath'] || !isset($options['encodePath'])) {
            $path = $this->pathMask->maskString($path);
        }

        $pathOptions = ['path' => $path];

        if (isset($options['maxWidth'])) {
            $pathOptions['maxWidth'] = (int)$options['maxWidth'];
        }

        if (isset($options['maxHeight'])) {
            $pathOptions['maxHeight'] = (int)$options['maxHeight'];
        }

        return $this->router->generate('_potato_media_image_get', $pathOptions);
    }
}