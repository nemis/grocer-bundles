<?php

namespace Potato\MediaBundle\Accessor;

use Potato\MediaBundle\Accessor\Exception\PathExistsException;
use Potato\MediaBundle\Accessor\Location\Path;
use Potato\MediaBundle\Accessor\Location\File;
use Potato\MediaBundle\Accessor\Exception\FileNotFoundException;
use Potato\MediaBundle\Accessor\Exception\LocationNotFoundException;

/**
 * Class FileSystem
 * @package Potato\MediaBundle\Accessor
 */
class FileSystem implements AccessorInterface
{
    /**
     * @param Path $path
     * @param File $file
     * @return mixed|string
     */
    private function getFullPath(Path $path, File $file = null)
    {
        $fullPath = str_replace(
            $path::SEPARATOR,
            DIRECTORY_SEPARATOR,
            $path->asString()
        );

        if (!is_null($file)) {
            $fullPath .= DIRECTORY_SEPARATOR.$file->asString();
        }

        return $fullPath;
    }

    /**
     * @param Path $path
     * @param File $file
     * @return File
     * @throws Exception\FileNotFoundException
     */
    public function load(Path $path, File $file)
    {
        $fullPath = $this->getFullPath($path, $file);

        if (!file_exists($fullPath)) {
            throw new FileNotFoundException($path->asString());
        }

        $file->setContents(file_get_contents($fullPath));

        return $file;
    }

    /**
     * @param Path $path
     * @param File $file
     * @throws Exception\LocationNotFoundException
     */
    public function store(Path $path, File $file)
    {
        $fullPath = $this->getFullPath($path);

        if (!is_dir($fullPath)) {
            throw new LocationNotFoundException($fullPath);
        }

        file_put_contents($fullPath.DIRECTORY_SEPARATOR.$file->getName(), $file->getContents());
    }

    /**
     * @param Path $path
     * @param $checkIfExists
     * @throws Exception\PathExistsException
     */
    private function createDirectory(Path $path, $checkIfExists)
    {
        $dir = $path->getName();

        if ($path->isAbsolute()) {
            $dir = DIRECTORY_SEPARATOR.$dir;
        }

        $fullPath = $this->getFullPath($path);

        if (file_exists($fullPath) && $checkIfExists) {
            throw new PathExistsException($fullPath);
        }

        if (!is_dir($dir)) {
            $umask = umask(0000);
            mkdir($dir, 0777);
            umask($umask);
        }

        while ($subPath = $path->getSubElement()) {
            $dir .= DIRECTORY_SEPARATOR.$subPath->getName();

            if (!is_dir($dir)) {
                $umask = umask(0000);
                mkdir($dir, 0777);
                umask($umask);
            }

            $path = $subPath;
        }
    }

    /**
     * @param Path $path
     */
    public function createPath(Path $path)
    {
        $this->createDirectory($path, true);
    }

    /**
     * @param Path $path
     */
    public function createPathIfNotExists(Path $path)
    {
        $this->createDirectory($path, false);
    }

    /**
     * @param Path $path
     * @throws Exception\LocationNotFoundException
     */
    public function deletePath(Path $path)
    {
        $fullPath = $this->getFullPath($path);

        if (!is_dir($fullPath)) {
            throw new LocationNotFoundException($path->asString());
        }

        rmdir($fullPath);
    }

    /**
     * @param Path $path
     * @param File $file
     * @throws Exception\FileNotFoundException
     * @throws Exception\LocationNotFoundException
     */
    public function deleteFile(Path $path, File $file)
    {
        $fullPath = $this->getFullPath($path);

        if (!is_dir($fullPath)) {
            throw new LocationNotFoundException($fullPath);
        }

        if (!is_file($file = $fullPath.DIRECTORY_SEPARATOR.$file->asString())) {
            throw new FileNotFoundException($file);
        }

        unlink($file);
    }

    /**
     * @param Path $path
     * @param File $file
     * @return bool
     */
    public function fileExists(Path $path, File $file)
    {
        return file_exists($path->asString().DIRECTORY_SEPARATOR.$file->asString());
    }
}