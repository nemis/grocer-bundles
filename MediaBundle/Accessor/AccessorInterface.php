<?php

namespace Potato\MediaBundle\Accessor;

use Potato\MediaBundle\Accessor\Location\Path;
use Potato\MediaBundle\Accessor\Location\File;

interface AccessorInterface
{
    public function load(Path $path, File $file);
    public function store(Path $path, File $file);
    public function createPath(Path $path);
    public function createPathIfNotExists(Path $path);
    public function deletePath(Path $path);
    public function deleteFile(Path $path, File $file);
    public function fileExists(Path $path, File $file);
}