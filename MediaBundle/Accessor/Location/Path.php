<?php

namespace Potato\MediaBundle\Accessor\Location;

/**
 * Class Path
 * @package Potato\MediaBundle\Accessor\Location
 */
class Path implements ElementInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var ElementInterface
     */
    private $subelement = null;

    /**
     * @var bool
     */
    private $absolute = false;

    const SEPARATOR = DIRECTORY_SEPARATOR;

    /**
     * @param string $name
     */
    public function __construct($name = '')
    {
        $this->name = $name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return boolean
     */
    public function isAbsolute()
    {
        return $this->absolute;
    }

    /**
     * @param $string
     * @return Path
     */
    public function parseString($string)
    {
        if (strlen($string) < 1) {
            return $this;
        }

        if ($string{0} == '/') {
            $string = substr($string, 1);
            $this->absolute = true;
        }

        $paths = explode(Path::SEPARATOR, $string);

        $path = $this;
        $this->setName(array_shift($paths));

        foreach ($paths as $pathName) {
            $path = $path->addSubPath($pathName);
        }

        return $this;
    }

    /**
     * @param string $name
     * @return Path
     */
    public function addSubPath($name)
    {
        $this->subelement = new Path($name);

        return $this->subelement;
    }

    /**
     * @param ElementInterface $element
     * @return ElementInterface
     */
    public function add(ElementInterface $element)
    {
        $this->subelement = $element;

        return $this->subelement;
    }

    /**
     * @return ElementInterface
     */
    public function getSubElement()
    {
        return $this->subelement;
    }

    /**
     * @return ElementInterface|Path
     */
    public function getLastElement()
    {
        $result = $this;

        $path = $this;

        while ($path instanceof Path && $element = $path->getSubElement()) {
            $result = $element;
            $path = $element;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function asString()
    {
        $result = $this->getName();

        if ($this->absolute) {
            $result = '/'.$result;
        }

        $path = $this;

        while ($path instanceof Path && $element = $path->getSubElement()) {
            $result .= (string)Path::SEPARATOR.$element->getName();
            $path = $element;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->asString();
    }
}