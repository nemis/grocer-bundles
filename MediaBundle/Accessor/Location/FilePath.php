<?php

namespace Potato\MediaBundle\Accessor\Location;

class FilePath
{
    /**
     * @var Path
     */
    private $path;

    /**
     * @var File
     */
    private $file;

    public function __construct(Path $path, File $file)
    {
        $this->path = $path;
        $this->file = $file;
    }

    /**
     * @param \Potato\MediaBundle\Accessor\Location\File $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return \Potato\MediaBundle\Accessor\Location\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param \Potato\MediaBundle\Accessor\Location\Path $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return \Potato\MediaBundle\Accessor\Location\Path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function asString()
    {
        return $this->path->asString().Path::SEPARATOR.$this->file->asString();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->asString();
    }
}