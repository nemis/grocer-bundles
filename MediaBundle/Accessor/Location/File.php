<?php

namespace Potato\MediaBundle\Accessor\Location;

/**
 * Class File
 * @package Potato\MediaBundle\Accessor\Location
 */
class File implements ElementInterface
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var null
     */
    private $contents;

    /**
     * @param string $name
     * @param null $contents
     */
    public function __construct($name = '', $contents = null)
    {
        $this->name = $name;
        $this->contents = $contents;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name =  $name;
    }

    /**
     * @param mixed $contents
     */
    public function setContents($contents)
    {
        $this->contents = $contents;
    }

    /**
     * @return mixed
     * @throws \DomainException
     */
    public function getContents()
    {
        if (!$this->contents) {
            throw new \DomainException('File has no content, you may use accessor to load it first.');
        }

        return $this->contents;
    }

    /**
     * @return string
     */
    public function asString()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->asString();
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        $dotName = explode('.', $this->name);

        return count($dotName) > 0 ? array_pop($dotName) : '';
    }

    /**
     * Stores a contents to tmp file
     * and returns it's name
     *
     * @param null $name
     * @return string
     */
    public function getTmpFile($name = null)
    {
        if (is_null($name)) {
            $name = uniqid().'_tmp.tmp';
        }

        $filename = sys_get_temp_dir().DIRECTORY_SEPARATOR.$name;

        file_put_contents($filename, $this->getContents());

        return $filename;
    }
}