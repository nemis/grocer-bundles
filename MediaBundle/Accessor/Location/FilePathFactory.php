<?php

namespace Potato\MediaBundle\Accessor\Location;

/**
 * Class FilePathFactory
 * @package Potato\MediaBundle\Accessor\Location
 */
class FilePathFactory
{
    /**
     * @param $string
     * @return FilePath
     */
    public function createFromString($string)
    {
        $stringExploded = explode('/', $string);

        $fileName = array_pop($stringExploded);

        $file = new File($fileName);

        $path = new Path();

        $path->parseString(implode(Path::SEPARATOR, $stringExploded));

        return new FilePath($path, $file);
    }

    /**
     * @param Path $path
     * @param File $file
     * @return FilePath
     */
    public function createFromPathAndFile(Path $path, File $file)
    {
        return new FilePath($path, $file);
    }
}