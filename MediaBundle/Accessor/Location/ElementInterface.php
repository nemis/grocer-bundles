<?php

namespace Potato\MediaBundle\Accessor\Location;

interface ElementInterface
{
    /**
     * @return string
     */
    public function __toString();

    /**
     * @return string
     */
    public function asString();

    /**
     * @param string $name
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();
}