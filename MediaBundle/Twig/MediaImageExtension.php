<?php

namespace Potato\MediaBundle\Twig;

use Potato\MediaBundle\Routing\Image;
use Symfony\Component\Routing\Router;

/**
 * Class MediaImageExtension
 * @package Potato\MediaBundle\Twig
 */
class MediaImageExtension extends \Twig_Extension
{
    /**
     * @var Image
     */
    private $router;

    /**
     * @param Image $router
     */
    public function __construct(Image $router)
    {
        $this->router = $router;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'potato_media_image' => new \Twig_Function_Method($this, 'image', array('is_safe' => array('html'))),
        );
    }

    /**
     * @param string $path
     * @param int $maxWidth
     * @param int $maxHeight
     * @param bool $encodePath
     * @return string
     */
    public function image($path = '', $maxWidth = null, $maxHeight = null, $encodePath = true)
    {
        if (!empty($path)) {
            $path = $this->router->get(
                $path,
                array(
                    'maxWidth' => $maxWidth,
                    'maxHeight' => $maxHeight,
                    'encodePath' => $encodePath
                )
            );
        }

        return '<img src="'.$path.'">';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'potato_media_image';
    }
}