<?php

namespace Potato\MediaBundle\Twig;

use Potato\MediaBundle\PathMask\PathMaskInterface;
use Symfony\Component\Routing\Router;

class PathExtension extends \Twig_Extension
{
    /**
     * @var \Potato\MediaBundle\PathMask\PathMaskInterface
     */
    private $pathMask;

    /**
     * @param PathMaskInterface $pathMask
     */
    public function __construct(PathMaskInterface $pathMask)
    {
        $this->pathMask = $pathMask;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'potato_media_path_mask' => new \Twig_Function_Method($this, 'mask'),
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'potato_media_path';
    }

    /**
     * @param $string
     * @return string
     */
    public function mask($string)
    {
        return $this->pathMask->maskString($string);
    }
}
