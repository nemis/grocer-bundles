(function(document) {
    "use strict"

    var PotatoMedia = {};
    PotatoMedia.config = {};

    PotatoMedia.setUp = function(config) {
        PotatoMedia.config = config;
    };

    PotatoMedia.getConfig = function(optionName, defaultValue) {
        if (!optionName) {
            return PotatoMedia.config;
        } else {
            if (typeof PotatoMedia.config[optionName] == 'undefined') {
                if (typeof defaultValue == 'undefined') {
                    throw "Config key not found: " + optionName;
                } else {
                    return defaultValue;
                }
            }

            return PotatoMedia.config[optionName];
        }
    };

    PotatoMedia.createCropDiv = function(src, cropWidth, cropHeight) {
        var cropDiv = document.createElement('div');
        cropWidth = cropWidth ? cropWidth : 100;
        cropHeight = cropHeight ? cropHeight : 100;

        cropDiv.className = 'potato-media-crop';
        cropDiv.setAttribute('data-width', cropWidth);
        cropDiv.setAttribute('data-height', cropHeight);
        cropDiv.style.width = cropWidth.toString() + 'px';
        cropDiv.style.height = cropHeight.toString() + 'px';
        cropDiv.style.overflow = 'hidden';
        cropDiv.style.backgroundImage = 'url(' + src + ')';
        cropDiv.style.backgroundRepeat = 'no-repeat';

        var img = document.createElement('img');
        img.src = src;

        var width;
        var height;

        img.onload = function() {
            width = img.width;
            height = img.height;

            var transX = img.width % cropWidth / 2;
            var transY = img.height % cropHeight / 2;

            cropDiv.style.backgroundPositionX = -transX.toString() + 'px';
            cropDiv.style.backgroundPositionY = -transY.toString() + 'px';
        };

        var grabbing = false;
        var currentMouseX = 0;
        var currentMouseY = 0;
        var prevMouseX = null;
        var prevMouseY = null;

        var grab = function(e) {
            e.preventDefault();

            grabbing = true;

            cropDiv.className = 'potato-media-crop grabbing';
        };

        var ungrab = function() {
            grabbing = false;
            cropDiv.className = 'potato-media-crop';
        };

        var moving = function(e) {
            currentMouseX = e.pageX - cropDiv.offsetLeft;
            currentMouseY = e.pageY - cropDiv.offsetTop;

            if (grabbing && prevMouseX && prevMouseY) {
                var diffX = prevMouseX - currentMouseX;
                var diffY = prevMouseY - currentMouseY;

                var currentBackgroundPositionX = parseInt(cropDiv.style.backgroundPositionX.replace('px', ''));
                var currentBackgroundPositionY = parseInt(cropDiv.style.backgroundPositionY.replace('px', ''));

                if (isNaN(currentBackgroundPositionX)) {
                    currentBackgroundPositionX = 0;
                }

                if (isNaN(currentBackgroundPositionY)) {
                    currentBackgroundPositionY = 0;
                }

                var newBackgroundPositionX = currentBackgroundPositionX - diffX;
                var newBackgroundPositionY = currentBackgroundPositionY - diffY;

                if (!(
                    newBackgroundPositionX < - width + cropWidth ||
                    newBackgroundPositionY < - height + cropHeight ||
                    newBackgroundPositionX > 0 ||
                    newBackgroundPositionY > 0
                )) {
                    cropDiv.style.backgroundPositionX = (newBackgroundPositionX).toString() + 'px';
                    cropDiv.style.backgroundPositionY = (newBackgroundPositionY).toString() + 'px';

                    containerDiv.onpositionchange.call(containerDiv, newBackgroundPositionX, newBackgroundPositionY);
                }
            }

            prevMouseX = currentMouseX;
            prevMouseY = currentMouseY;
        };

        cropDiv.onmousedown = grab;
        document.body.onmousemove = moving;
        document.body.onmouseup = ungrab;
        document.body.onclick = ungrab;

        var containerDiv = document.createElement('div');
        containerDiv.appendChild(cropDiv);

        containerDiv.onpositionchange = function(x, y) {};

        return containerDiv;
    };

    PotatoMedia.createForm = function(action) {
        var form = document.createElement('form');
        form.setAttribute('enctype', 'multipart/form-data');
        form.setAttribute('action', action);
        form.setAttribute('method', 'post');

        return form;
    };

    PotatoMedia.createEmptyIframe = function() {
        var iframe = document.createElement('iframe');
        iframe.width = 200;
        iframe.height = 25;
        iframe.style.overflow = 'hidden';
        iframe.scrolling = 'no';
        iframe.style.border = '0px';
        iframe.style.margin = '0px';
        iframe.style.padding = '0px';
        iframe.src = 'about:blank';

        return iframe;
    };

    PotatoMedia.waitForIframe = function(iframe, ready) {
        var doc;
        var repeat = true;
        var lookForDoc = function() {
            if (iframe.contentDocument) { // FF Chrome
                doc = iframe.contentDocument;
                repeat = false;
            } else if ( iframe.contentWindow ) { // IE
                doc = iframe.contentWindow.document;
                repeat = false;
            } else {
                repeat = true;
            }
            if (repeat) {
                setTimeout(lookForDoc, 500);
            } else {
                ready.call(null, doc);
            }
        };

        setTimeout(lookForDoc, 500);
    };

    PotatoMedia.generateImgCropField = function(field, containerWidth, containerHeight) {
        var originalName = field.getAttribute('name');

        field.style.display = 'none';

        var iframe = PotatoMedia.createEmptyIframe();

        var fieldParentNode = field.parentNode;
        fieldParentNode.appendChild(iframe);

        var imgCrop = {};
        imgCrop.onpositionchange = function(x, y) {};
        imgCrop.onupload = function(file) {};
        imgCrop.onsubmit = function() {};

        var ready = function(doc) {
            var form = PotatoMedia.createForm(PotatoMedia.getConfig('tmpUploadUrl'));

            var buildIframeBody = function() {
                var containerWidth = containerWidth ? containerWidth : 100;
                var containerHeight = containerHeight ? containerHeight : 100;
                var containerWidthInput = document.createElement('input');
                var containerHeightInput = document.createElement('input');
                var typeField = document.createElement('input');
                typeField.name = 'type';
                typeField.value = 'image';
                containerWidthInput.name = 'containerWidth';
                containerWidthInput.value = containerWidth;
                containerHeightInput.name = 'containerHeight';
                containerHeightInput.value = containerHeight;

                form.appendChild(field);
                form.appendChild(containerWidthInput);
                form.appendChild(containerHeightInput);
                form.appendChild(typeField);

                field.style.display = 'inline';
                field.name = 'file';
            };

            iframe.onload = function() {
                var cropDiv = PotatoMedia.createCropDiv(
                    PotatoMedia.getConfig('tmpUploadResult'),
                    containerWidth,
                    containerHeight
                );

                fieldParentNode.appendChild(cropDiv);

                cropDiv.onpositionchange = function(x, y) {
                    imgCrop.onpositionchange.call(imgCrop, -x, -y);
                };

                (function lookupForSrc() {
                    var doc = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;
                    if (doc.getElementsByTagName('form').length > 0) {
                        setTimeout(lookupForSrc, 500);
                    } else {
                        var file = doc.body.innerHTML;
                        imgCrop.onupload.call(imgCrop, file);
                    }
                })();
            };

            field.onchange = function() {
                iframe.style.display = 'none';
                imgCrop.onsubmit.call(imgCrop);
                form.submit();
            };

            buildIframeBody();
        };

        PotatoMedia.waitForIframe(iframe, ready);
        return imgCrop;
    };

    PotatoMedia.generateIframeUpload = function(field, action, callback) {
        var iframe = PotatoMedia.createEmptyIframe();
        field.style.display = 'none';

        var fieldParentNode = field.parentNode;
        fieldParentNode.appendChild(iframe);

        var form = PotatoMedia.createForm(action);
        form.method = 'post';
        form.enctype = 'multipart/form-data';
        form.appendChild(field);

        var createIframeField = function(doc) {
            doc.body.innerHTML = '';
            doc.body.appendChild(form);
            doc.body.style.padding = '0px';
            doc.body.style.margin = '0px';

            doc.body.appendChild(form);

            field.style.display = 'inline';
            field.onchange = function() {
                form.submit();
            };

            iframe.onload = function() {
                var doc = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;
                var result = doc.body.innerHTML;
                field.value = '';
                if (callback) {
                    callback.call(this, result);
                }

                createIframeField(doc);
            };
        };

        PotatoMedia.waitForIframe(iframe, createIframeField);

        iframe.addOption = function(name, value) {
            var input = document.createElement('input');
            input.name = name;
            input.value = value;
            input.type = "hidden";

            form.appendChild(input);
        };

        return iframe;
    };

    window.PotatoMedia = PotatoMedia;
})(document);