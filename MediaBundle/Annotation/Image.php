<?php

namespace Potato\MediaBundle\Annotation;

/**
 * Class Image
 * @package Potato\MediaBundle\Annotation
 *
 * @Annotation
 */
class Image implements AnnotationInterface
{
    /**
     * @var int
     */
    public $maxWidth;
    
    /**
     * @var int
     */
    public $maxHeight;
}