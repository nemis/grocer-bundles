<?php

namespace Potato\MediaBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraints\Image as Image;

class MediaImage extends Image
{
    public function validatedBy()
    {
        return 'media_image';
    }
}