<?php

namespace Potato\MediaBundle\Validator\Constraints;

use Potato\MediaBundle\Accessor\AccessorInterface;
use Potato\MediaBundle\Accessor\Location\FilePathFactory;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\ImageValidator;

/**
 * Class MediaImageValidator
 * @package Potato\MediaBundle\Validator\Constraints
 */
class MediaImageValidator extends ImageValidator
{
    /**
     * @var AccessorInterface
     */
    private $accessor;

    /**
     * @var FilePathFactory
     */
    private $filePathFactory;

    /**
     * @param AccessorInterface $accessor
     * @param FilePathFactory $filePathFactory
     */
    public function __construct(AccessorInterface $accessor, FilePathFactory $filePathFactory)
    {
        $this->accessor = $accessor;
        $this->filePathFactory = $filePathFactory;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     * @return bool
     */
    public function validate($value, Constraint $constraint)
    {
        if (is_string($value)) {
            $filePath = $this->filePathFactory->createFromString($value);

            if ($this->accessor->fileExists($filePath->getPath(), $filePath->getFile())) {
                return;
            }
        }

        parent::validate($value, $constraint);
    }
}