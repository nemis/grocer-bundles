<?php

namespace Potato\MediaBundle\Element;

use Potato\MediaBundle\Accessor\Location\File;
use Potato\MediaBundle\Accessor\Location\Path;

class AbstractElement
{
    /**
     * @var File
     */
    protected $file;

    /**
     * @var Path
     */
    protected $path;

    /**
     * @param Path $path
     * @param File $file
     */
    public function __construct(Path $path = null, File $file = null)
    {
        $this->file = $file;
        $this->path = $path;
    }

    /**
     * @param \Potato\MediaBundle\Accessor\Location\File $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return \Potato\MediaBundle\Accessor\Location\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param \Potato\MediaBundle\Accessor\Location\Path $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return \Potato\MediaBundle\Accessor\Location\Path
     */
    public function getPath()
    {
        return $this->path;
    }
}