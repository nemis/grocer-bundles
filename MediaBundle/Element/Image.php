<?php

namespace Potato\MediaBundle\Element;

/**
 * Class Image
 * @package Potato\MediaBundle\Element
 */
class Image extends AbstractElement
{
    /**
     * @return string
     */
    public function getMime()
    {
        $finfo = new \finfo(FILEINFO_MIME);

        $mime = $finfo->file($this->getFile()->getTmpFile());

        if (strpos($mime, ';') > -1) {
            $mimeExploded = explode(';', $mime);

            $mime = $mimeExploded[0];
        }

        return $mime;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        $tmpfile = $this->getFile()->getTmpFile();

        $size = getimagesize($tmpfile);
        return $size[0];
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        $tmpfile = $this->getFile()->getTmpFile();

        $size = getimagesize($tmpfile);
        return $size[1];
    }

    /**
     * @return bool|string
     */
    public function getExtensionFromMime()
    {
        $mime = $this->getMime();

        switch($mime)
        {
            case 'image/bmp':
                return 'bmp';
            case 'image/cis-cod':
                return 'cod';
            case 'image/gif':
                return 'gif';
            case 'image/ief':
                return 'ief';
            case 'image/jpeg':
                return 'jpg';
            case 'image/pipeg':
                return 'jfif';
            case 'image/tiff':
                return 'tif';
            case 'image/x-cmu-raster':
                return 'ras';
            case 'image/x-cmx':
                return 'cmx';
            case 'image/x-icon':
                return 'ico';
            case 'image/x-portable-anymap':
                return 'pnm';
            case 'image/x-portable-bitmap':
                return 'pbm';
            case 'image/x-portable-graymap':
                return 'pgm';
            case 'image/x-portable-pixmap':
                return 'ppm';
            case 'image/x-rgb':
                return 'rgb';
            case 'image/x-xbitmap':
                return 'xbm';
            case 'image/x-xpixmap':
                return 'xpm';
            case 'image/x-xwindowdump':
                return 'xwd';
            case 'image/png':
                return 'png';
            case 'image/x-jps':
                return 'jps';
            case 'image/x-freehand':
                return 'fh';
            default:
                return false;
        }
    }
}