<?php

namespace Potato\MediaBundle\Element\Factory;

use Potato\MediaBundle\Accessor\Location\FilePath;
use Potato\MediaBundle\Accessor\Location\FilePathFactory;
use Potato\MediaBundle\Element\Image;

/**
 * Class ImageFactory
 * @package Potato\MediaBundle\Element\Factory
 */
class ImageFactory
{
    /**
     * @var FilePathFactory
     */
    private $filePathFactory;

    /**
     * @param FilePathFactory $filePathFactory
     */
    public function __construct(FilePathFactory $filePathFactory)
    {
        $this->filePathFactory = $filePathFactory;
    }

    /**
     * @param $string
     * @return Image
     */
    public function createFromPathString($string)
    {
        $path = $this->filePathFactory->createFromString($string);

        return new Image($path->getPath(), $path->getFile());
    }

    /**
     * @param FilePath $filePath
     * @return Image
     */
    public function createFromFilePath(FilePath $filePath)
    {
        return new Image($filePath->getPath(), $filePath->getFile());
    }
}