<?php

namespace Potato\MediaBundle\Manipulation\Data;

interface DataFactoryInterface
{
    /**
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     * @return Crop
     */
    public function createCropData($x, $y, $width, $height);
}
