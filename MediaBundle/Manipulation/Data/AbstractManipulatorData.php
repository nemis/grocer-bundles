<?php

namespace Potato\MediaBundle\Manipulation\Data;

/**
 * Class AbstractManipulatorData
 * @package Potato\MediaBundle\Manipulation\Data
 */
abstract class AbstractManipulatorData
{
    /**
     * @var mixed
     */
    private $value;

    /**
     * @param mixed $value
     */
    public function setOriginalValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getOriginalValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->value;
    }
}