<?php

namespace Potato\MediaBundle\Manipulation\Data;

class DataFactory implements DataFactoryInterface
{
    /**
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     * @return Crop
     */
    public function createCropData($x, $y, $width, $height)
    {
        return new Crop($x, $y, $width, $height);
    }
}