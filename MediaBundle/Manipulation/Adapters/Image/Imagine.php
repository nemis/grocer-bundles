<?php

namespace Potato\MediaBundle\Manipulation\Adapters\Image;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Image\Point;
use Potato\MediaBundle\Element\Image;
use Imagine\Gd\Imagine as GdImagine;

/**
 * Class Imagine
 * @package Potato\MediaBundle\Manipulation\Adapters\Image
 */
class Imagine implements ImageAdapterInterface
{
    /**
     * @param int $width
     * @param int $height
     * @param int $maxWidth
     * @param int $maxHeight
     * @param bool $forCrop
     * @return array
     */
    private function calculateRatio($width, $height, $maxWidth, $maxHeight, $forCrop = false)
    {
        if (($forCrop && $width < $height) || (!$forCrop && $width > $height)) {
            $desiredWidth = $maxWidth;
            $scalingFactor = $desiredWidth / $width;
            $desiredHeight = $height * $scalingFactor;
        } else {
            $desiredHeight = $maxHeight;
            $scalingFactor = $desiredHeight / $height;
            $desiredWidth = $width * $scalingFactor;
        }

        return array($desiredWidth, $desiredHeight);
    }

    /**
     * @param Image $image
     * @param int $width
     * @param int $height
     * @param bool $forCrop
     */
    public function resize(Image $image, $width, $height, $forCrop = false)
    {
        $imagineImage = new GdImagine();

        $fileName = uniqid().'.'.$image->getExtensionFromMime();

        $tmpfile = $image->getFile()->getTmpFile($fileName);

        $size = getimagesize($tmpfile);

        list($newWidth, $newHeight) = $this->calculateRatio($size[0], $size[1], $width, $height, $forCrop);

        $imagineImage
            ->open($tmpfile)
            ->resize(new Box($newWidth, $newHeight))
            ->save($tmpfile);

        $image->getFile()->setContents(file_get_contents($tmpfile));
    }

    /**
     * @param Image $image
     * @param string $desiredExtension
     */
    public function convert(Image $image, $desiredExtension)
    {
        $imagineImage = new GdImagine();

        $tmpfile = $image->getFile()->getTmpFile();

        $newTmpFile = $tmpfile.'.'.$desiredExtension;

        $imagineImage
            ->open($tmpfile)
            ->save($newTmpFile);

        $image->getFile()->setContents(file_get_contents($newTmpFile));
    }

    /**
     * @param Image $image
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     */
    public function crop(Image $image, $x, $y, $width, $height)
    {
        $imagineImage = new GdImagine();

        $tmpfile = $image->getFile()->getTmpFile('crop_file_'.uniqid().'.'.$image->getExtensionFromMime());

        $imagineImage
            ->open($tmpfile)
            ->crop(new Point($x, $y), new Box($width, $height))
            ->save($tmpfile);

        $image->getFile()->setContents(file_get_contents($tmpfile));
    }
}