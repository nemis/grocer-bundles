<?php

namespace Potato\MediaBundle\Manipulation\Adapters\Image;

use Potato\MediaBundle\Element\Image;

interface ImageAdapterInterface
{
    public function resize(Image $image, $width, $height, $forCrop = false);
    public function convert(Image $image, $desiredExtension);
    public function crop(Image $image, $x, $y, $width, $height);
}