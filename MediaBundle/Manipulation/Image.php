<?php

namespace Potato\MediaBundle\Manipulation;

use Potato\MediaBundle\Element\Image as ImageElement;
use Potato\MediaBundle\Manipulation\Adapters\Image\ImageAdapterInterface;

/**
 * Class Image
 * @package Potato\MediaBundle\Services
 */
class Image
{
    /**
     * @var Adapters\Image\ImageAdapterInterface
     */
    private $adapter;

    /**
     * @param ImageAdapterInterface $adapter
     */
    public function __construct(ImageAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * Convert image to desired extension
     */

    /**
     * @param ImageElement $image
     * @param int $width
     * @param int $height
     * @param bool $forCrop
     */
    public function resize(ImageElement $image, $width, $height, $forCrop = false)
    {
        $this->adapter->resize($image, $width, $height, $forCrop);
    }

    /**
     * @param ImageElement $image
     * @param string $desiredExtension
     */
    public function convert(ImageElement $image, $desiredExtension)
    {
        $this->adapter->convert($image, $desiredExtension);
    }

    /**
     * @param ImageElement $image
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     */
    public function crop(ImageElement $image, $x, $y, $width, $height)
    {
        $this->adapter->crop($image, $x, $y, $width, $height);
    }
}