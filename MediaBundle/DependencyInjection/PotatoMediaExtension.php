<?php

namespace Potato\MediaBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class PotatoMediaExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('doctrine.yml');
        $loader->load('processors.yml');
        $loader->load('form_types.yml');
        $loader->load('twig.yml');

        $this->setUploadDir($config, $container);
        $this->setDesiredExtension($config, $container);
        $this->addFormFieldsTemplates($container);
    }

    /**
     * @param array $config
     * @param ContainerBuilder $container
     */
    private function setUploadDir($config, ContainerBuilder $container)
    {
        if (isset($config['base_dir'])) {
            $container->setParameter('potato.media.base_dir', $config['base_dir']);
        }
    }

    /**
     * @param array $config
     * @param ContainerBuilder $container
     */
    private function setDesiredExtension($config, ContainerBuilder $container)
    {
        if (isset($config['desired_image_extension'])) {
            $container->setParameter('potato.media.image.desired_extension', $config['desired_image_extension']);
        }
    }

    /**
     * @param ContainerBuilder $container
     */
    private function addFormFieldsTemplates(ContainerBuilder $container)
    {

        $container->setParameter('twig.form.resources', array_merge(
            $container->getParameter('twig.form.resources'),
            array('PotatoMediaBundle:Form:fields.html.twig')
        ));
    }
}
