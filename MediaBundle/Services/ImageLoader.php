<?php

namespace Potato\MediaBundle\Services;

use Potato\MediaBundle\Accessor\AccessorInterface;
use Potato\MediaBundle\Element\Factory\ImageFactory;
use Potato\MediaBundle\Manipulation\Image;
use Potato\MediaBundle\Element\Image as ImageElement;

/**
 * Class ImageLoader
 * @package Potato\MediaBundle\Services
 */
class ImageLoader
{
    const DEFAULT_MAX_WIDTH = 2000;
    const DEFAULT_MAX_HEIGHT = 2000;
    const MIN_WIDTH = 10;
    const MIN_HEIGHT = 10;

    /**
     * @var \Potato\MediaBundle\Accessor\AccessorInterface
     */
    private $accessor;

    /**
     * @var \Potato\MediaBundle\Manipulation\Image
     */
    private $manipulation;

    /**
     * @var \Potato\MediaBundle\Element\Factory\ImageFactory
     */
    private $imageFactory;

    /**
     * @param AccessorInterface $accessor
     * @param Image $manipulation
     * @param ImageFactory $imageFactory
     */
    public function __construct(AccessorInterface $accessor, Image $manipulation, ImageFactory $imageFactory)
    {
        $this->accessor = $accessor;
        $this->manipulation = $manipulation;
        $this->imageFactory = $imageFactory;
    }

    /**
     * @param ImageElement $image
     * @param int $width
     * @param int $height
     */
    private function modifyFileNameBySize(ImageElement $image, $width, $height)
    {
        $sizePrefix = (int)$width.'_'.(int)$height;
        $image->getFile()->setName($sizePrefix.'_'.$image->getFile()->getName());
    }

    /**
     * @param ImageElement $image
     * @param null $maxWidth
     * @param null $maxHeight
     */
    private function resizeImage(ImageElement $image, $maxWidth = null, $maxHeight = null)
    {
        if ($maxWidth < self::MIN_WIDTH) {
            $maxWidth = self::DEFAULT_MAX_WIDTH;
        }

        if ($maxHeight < self::MIN_HEIGHT) {
            $maxHeight = self::DEFAULT_MAX_HEIGHT;
        }

        $this->manipulation->resize($image, $maxWidth, $maxHeight);
    }

    /**
     * @param $path
     * @param int $maxWidth
     * @param int $maxHeight
     * @return \Potato\MediaBundle\Element\Image
     */
    public function loadFromString($path, $maxWidth = null, $maxHeight = null)
    {
        $image = $this->imageFactory->createFromPathString($path);
        $this->accessor->load($image->getPath(), $image->getFile());

        if (!($maxWidth < self::MIN_WIDTH && $maxHeight < self::MIN_HEIGHT)) {
            $this->modifyFileNameBySize($image, $maxWidth, $maxHeight);

            if ($this->accessor->fileExists($image->getPath(), $image->getFile())) {
                $this->accessor->load($image->getPath(), $image->getFile());
            } else {
                $this->resizeImage($image, $maxWidth, $maxHeight);
                $this->accessor->store($image->getPath(), $image->getFile());
            }
        }

        return $image;
    }
}