<?php

namespace Potato\MediaBundle\Form\Type;

use Potato\MediaBundle\Element\Factory\ImageFactory;
use Potato\MediaBundle\Manipulation\Data\DataFactory;
use Potato\MediaBundle\Manipulation\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ImageCropType
 */
class ImageCropType extends AbstractType
{
    /**
     * @var string
     */
    private $filePreviewPath = null;

    /**
     * @var DataFactory
     */
    private $dataFactory;

    /**
     * @param DataFactory $dataFactory
     */
    public function __construct(DataFactory $dataFactory)
    {
        $this->dataFactory = $dataFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($options) {
            $data = $event->getData();

            if (!empty($data['position'])) {
                $cropData = $this->dataFactory->createCropData(
                    (int)$data['position']['x'],
                    (int)$data['position']['y'],
                    (int)$data['width'],
                    (int)$data['height']
                );

                $cropData->setOriginalValue($data['file']);

                $event->setData($cropData);
            } else {
                $event->setData($data['old_file_path']);
            }
        });

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($options) {
            $data = $event->getData();

            $this->filePreviewPath = $data;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['file_preview_path'] = $this->filePreviewPath;
        $view->vars['uniqid'] = uniqid();
        $view->vars['width'] = $options['width'];
        $view->vars['height'] = $options['height'];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'potato_media_image_crop_type';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'file';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'width' => 100,
            'height' => 100
        ));
    }
}