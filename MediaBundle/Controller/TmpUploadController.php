<?php

namespace Potato\MediaBundle\Controller;

use Imagine\Exception\RuntimeException;
use Potato\MediaBundle\Accessor\AccessorInterface;
use Potato\MediaBundle\Accessor\Location\FilePath;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Potato\MediaBundle\Accessor\Exception\FileNotFoundException as AccessorFileNotFound;

/**
 * Class TmpUploadController
 * @package Potato\MediaBundle\Controller
 */
class TmpUploadController extends ContainerAware
{
    /**
     * @return FilePath
     */
    private function getTmpFilePath()
    {
        $id = '';

        if (is_object(
            $this
                ->container
                ->get('security.context')
                ->getToken()
                ->getUser()
        )) {
            $id .= $this
                ->container
                ->get('security.context')
                ->getToken()
                ->getUser()
                ->getId().'_';
        }

        $id .= uniqid();

        $baseDir = $this->container->getParameter('potato.media.base_dir');

        $filePath = $this
            ->container
            ->get('potato.media.accessor.location.filepath_factory')
            ->createFromString($baseDir.'/tmpupload/'.$id);

        return $filePath;
    }

    /**
     * @return \Potato\MediaBundle\Element\Factory\ImageFactory
     */
    private function getImageFactory()
    {
        return $this->container->get('potato.media.element.factory.image');
    }

    /**
     * @return AccessorInterface
     */
    private function getAccessor()
    {
        return $this->container->get('potato.media.accessor');
    }

    /**
     * @param Request $request
     * @return FilePath
     * @throws \Symfony\Component\HttpFoundation\File\Exception\UploadException
     * @throws \Imagine\Exception\RuntimeException
     */
    private function storeFile(Request $request)
    {
        $files = $request->files;

        if (count($files) < 0) {
            throw new UploadException('File cannot be empty.');
        }

        $tmpFileDir = sys_get_temp_dir();
        $tmpFileName = uniqid('upload_');

        $files = iterator_to_array($files);

        $file = array_shift($files);

        if (!$file instanceof UploadedFile) {
            throw new UploadException('No file input found.');
        }

        $file->move($tmpFileDir, $tmpFileName);

        $filePath = $this->getTmpFilePath();
        $filePath->getFile()->setContents(file_get_contents($tmpFileDir.DIRECTORY_SEPARATOR.$tmpFileName));

        $accessor = $this->container->get('potato.media.accessor');
        $accessor->createPathIfNotExists($filePath->getPath());
        $accessor->store($filePath->getPath(), $filePath->getFile());

        return new Response($filePath->asString());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function uploadAction(Request $request)
    {
        $filePath = $this->storeFile($request);
        $image = $this->getImageFactory()->createFromFilePath($filePath);

        $containerWidth = $request->get('containerWidth') ? $request->get('containerWidth') : false;
        $containerHeight = $request->get('containerHeight') ? $request->get('containerHeight') : false;
        $type = $request->get('type');

        if ($containerWidth && $containerHeight && $type == 'image') {
            $this->container
                ->get('potato.media.manipulation.image')
                ->resize(
                    $image,
                    $containerWidth,
                    $containerHeight,
                    true
                );

            $this->getAccessor()->store($image->getPath(), $image->getFile());
        }

        return new Response(
            $this
                ->container
                ->get('potato.media.accessor.location.filepath_factory')
                ->createFromPathAndFile($image->getPath(), $image->getFile())
                ->asString()
        );
    }

    /**
     * @return Response
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    public function getTmpFileAction()
    {
        $accessor = $this->getAccessor();
        $filePath = $this->getTmpFilePath();

        try {
            $accessor->load($filePath->getPath(), $filePath->getFile());
        } catch (AccessorFileNotFound $e) {
            throw new FileNotFoundException($filePath->asString());
        }

        $image = $this->getImageFactory()->createFromFilePath($filePath);

        $headers = array(
            'Content-Type'=> $image->getMime(),
            'Content-Disposition' => 'inline; filename="'.$image->getFile()->getName().'"'
        );

        return new Response($image->getFile()->getContents(), 200, $headers);
    }
}