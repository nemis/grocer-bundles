<?php

namespace Potato\MediaBundle\Controller;

use Potato\MediaBundle\Accessor\Exception\FileNotFoundException;
use Potato\MediaBundle\Twig\MediaImageExtension;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException as SymfonyFileNotFoundException;

/**
 * Class ImageController
 * @package Potato\MediaBundle\Controller
 */
class ImageController extends ContainerAware
{
    /**
     * @return \Potato\MediaBundle\Element\Factory\ImageFactory
     */
    private function getImageFactory()
    {
        return $this->container->get('potato.media.element.factory.image');
    }

    /**
     * @return \Potato\MediaBundle\Accessor\AccessorInterface
     */
    private function getAccessor()
    {
        return $this->container->get('potato.media.accessor');
    }

    /**
     * @param Request $request
     * @return Response
     * @throws SymfonyFileNotFoundException
     */
    public function getAction(Request $request)
    {
        $path = $request->get('path');
        $maxWidth = $request->get('maxWidth');
        $maxHeight = $request->get('maxHeight');

        $path = $this->container->get('potato.media.path_mask')->unmaskString($path);

        $image = null;

        try {
            $image = $this->container->get('potato.media.image_loader')->loadFromString($path, $maxWidth, $maxHeight);
        } catch (FileNotFoundException $e) {
            throw new SymfonyFileNotFoundException($path);
        }

        $imageBody = $image->getFile()->getContents();

        $file = $image->getFile()->getName();
        $headers = array(
            'Content-Type' => $image->getMime(),
            'Content-Disposition' => 'inline; filename="'.$file.'"'
        );

        return new Response($imageBody, 200, $headers);
    }
}