<?php

namespace Potato\MediaBundle\Processors;

use Potato\MediaBundle\Accessor\AccessorInterface;
use Potato\MediaBundle\Accessor\Location\FilePathFactory;
use Potato\MediaBundle\Annotation\AnnotationInterface;
use Potato\MediaBundle\Annotation\Image as ImageAnnotation;
use Potato\MediaBundle\Element\Factory\ImageFactory;
use Potato\MediaBundle\Manipulation\Data\AbstractManipulatorData;
use Potato\MediaBundle\Manipulation\Data\Crop;
use Potato\MediaBundle\Manipulation\Image as ImageManipulator;
use Potato\MediaBundle\PathTranslation\AbstractTranslator;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Potato\MediaBundle\Element\Image as ImageElement;
use Symfony\Component\HttpFoundation\File\UploadedFile as UploadedFile;

/**
 * Class Image
 * @package Potato\MediaBundle\Processors
 */
class Image implements ProcessorInterface
{
    /**
     * @var \Potato\MediaBundle\Manipulation\Image
     */
    private $imageManipulator;

    /**
     * @var \Potato\MediaBundle\Element\Factory\ImageFactory
     */
    private $imageFactory;

    /**
     * @var \Potato\MediaBundle\Accessor\AccessorInterface
     */
    private $accessor;

    /**
     * @var \Potato\MediaBundle\PathTranslation\AbstractTranslator
     */
    private $pathTranslator;

    /**
     * @var string
     */
    private $desiredExtension;

    /**
     * @var \Symfony\Component\PropertyAccess\PropertyAccessor
     */
    private $propertyAccessor;

    /**
     * @param ImageManipulator $imageManipulator
     * @param ImageFactory $imageFactory
     * @param AccessorInterface $accessor
     * @param AbstractTranslator $pathTranslator
     * @param FilePathFactory $pathFactory
     * @param string $desiredExtension
     */
    public function __construct(
        ImageManipulator $imageManipulator,
        ImageFactory $imageFactory,
        AccessorInterface $accessor,
        AbstractTranslator $pathTranslator,
        FilePathFactory $pathFactory,
        $desiredExtension = 'jpg'
    ) {
        $this->imageManipulator = $imageManipulator;
        $this->imageFactory = $imageFactory;
        $this->accessor = $accessor;
        $this->pathTranslator = $pathTranslator;
        $this->pathFactory = $pathFactory;
        $this->desiredExtension = $desiredExtension;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @param AnnotationInterface $annotation
     * @return bool
     */
    private function isSizeDeclared(AnnotationInterface $annotation)
    {
        return
            $this->propertyAccessor->getValue($annotation, 'maxWidth') != null
            &&
            $this->propertyAccessor->getValue($annotation, 'maxHeight') != null;
    }

    /**
     * @param mixed $entity
     * @param string $property
     * @return ImageElement
     */
    private function createImage($entity, $property)
    {
        $path = $this->propertyAccessor->getValue($entity, $property);

        $image = $this->imageFactory->createFromPathString($path);

        $image->getFile()->setContents(file_get_contents($path));

        return $image;
    }

    /**
     * @param ImageElement $image
     * @param AnnotationInterface $annotation
     * @param object $entity
     * @param string $property
     */
    private function processManipulationData(ImageElement $image, AnnotationInterface $annotation, $entity, $property)
    {
        $value = $this->propertyAccessor->getValue($entity, $property);

        if ($value instanceof AbstractManipulatorData) {
            if ($value instanceof Crop) {
                $this->imageManipulator->crop(
                    $image,
                    $value->getX(),
                    $value->getY(),
                    $value->getWidth(),
                    $value->getHeight()
                );
            }

            $value = $value->getOriginalValue();
        }

        $this->propertyAccessor->setValue($entity, $property, $value);
    }

    /**
     * @param ImageElement $image
     * @param AnnotationInterface $annotation
     * @param object $entity
     * @param string $property
     */
    private function processUploadedFile(ImageElement $image, AnnotationInterface $annotation, $entity, $property)
    {
        $this->convertImage($image);

        if ($this->isSizeDeclared($annotation)) {
            $this->resize(
                $image,
                $this->propertyAccessor->getValue($annotation, 'maxWidth'),
                $this->propertyAccessor->getValue($annotation, 'maxHeight')
            );
        }

        $this->moveUploadedImageFile($image, $entity, $property);
    }

    /**
     * @param AnnotationInterface $annotation
     * @param object $entity
     * @param string $property
     */
    public function processPersist(AnnotationInterface $annotation, $entity, $property)
    {
        if ($annotation instanceof ImageAnnotation) {
            $value = $this->propertyAccessor->getValue($entity, $property);

            if (empty($value)) {
                return;
            }

            $image = $this->createImage($entity, $property);

            if ($value instanceof AbstractManipulatorData) {
                $this->processManipulationData($image, $annotation, $entity, $property);
                $this->processUploadedFile($image, $annotation, $entity, $property);
            }

            if ($value instanceof UploadedFile) {
                $this->processUploadedFile($image, $annotation, $entity, $property);
            }
        }
    }

    /**
     * @param AnnotationInterface $annotation
     * @param $entity
     * @param $property
     */
    public function processRemove(AnnotationInterface $annotation, $entity, $property)
    {
        if ($annotation instanceof ImageAnnotation) {
            $value = $this->propertyAccessor->getValue($entity, $property);

            if (empty($value)) {
                return;
            }

            $filePath = $this->pathFactory->createFromString($value);

            $this->accessor->deleteFile($filePath->getPath(), $filePath->getFile());
        }
    }

    /**
     * @param ImageElement $image
     * @param object $entity
     * @param string $property
     */
    private function moveUploadedImageFile(ImageElement $image, $entity, $property)
    {
        $filePath = $this->pathFactory->createFromPathAndFile($image->getPath(), $image->getFile());

        $newFilePath = $this->pathTranslator->translate($entity, $property, $filePath, $this->desiredExtension);

        $image->setPath($newFilePath->getPath());
        $image->setFile($newFilePath->getFile());

        $this->accessor->createPathIfNotExists($newFilePath->getPath());

        $this->accessor->store($newFilePath->getPath(), $newFilePath->getFile());

        $this->propertyAccessor->setValue($entity, $property, $newFilePath->asString());
    }

    private function convertImage(ImageElement $image)
    {
        $this->imageManipulator->convert($image, $this->desiredExtension);
    }

    /**
     * @param ImageElement $image
     * @param $maxWidth
     * @param $maxHeight
     */
    private function resize(ImageElement $image, $maxWidth, $maxHeight)
    {
        $this->imageManipulator->resize($image, $maxWidth, $maxHeight, $this->desiredExtension);
    }
}
