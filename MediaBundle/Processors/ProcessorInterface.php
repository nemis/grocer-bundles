<?php

namespace Potato\MediaBundle\Processors;

use Potato\MediaBundle\Annotation\AnnotationInterface;

interface ProcessorInterface
{
    public function processPersist(AnnotationInterface $annotation, $entity, $property);
    public function processRemove(AnnotationInterface $annotation, $entity, $property);
}