<?php

namespace Potato\MediaBundle\Processors\Container;

use Potato\MediaBundle\Annotation\AnnotationInterface;
use Potato\MediaBundle\Processors\ProcessorInterface;

/**
 * Class Container
 * @package Potato\MediaBundle\Processors\Container
 */
class Container implements ProcessorInterface
{
    /**
     * @var ProcessorInterface[]
     */
    private $processors;

    /**
     * @param ProcessorInterface $processor
     */
    public function addProcessor(ProcessorInterface $processor)
    {
        $this->processors[] = $processor;
    }

    /**
     * @param AnnotationInterface $annotation
     * @param mixed $entity
     * @param mixed $property
     */
    public function processPersist(AnnotationInterface $annotation, $entity, $property)
    {
        foreach ($this->processors as $processor) {
            $processor->processPersist($annotation, $entity, $property);
        }
    }

    /**
     * @param AnnotationInterface $annotation
     * @param mixed $entity
     * @param mixed $property
     */
    public function processRemove(AnnotationInterface $annotation, $entity, $property)
    {
        foreach ($this->processors as $processor) {
            $processor->processRemove($annotation, $entity, $property);
        }
    }
}