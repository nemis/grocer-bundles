<?php

namespace Potato\MediaBundle\Tests\PathMask;

use Potato\MediaBundle\PathMask\SecretKeyMask;
use \Mockery as m;

class SecretKeyTest extends \PHPUnit_Framework_TestCase
{
    public function testEncodesAndDecodesString()
    {
        $key = 'I am secret key.';
        $string = 'secret string';

        $secretKeyMask = new SecretKeyMask($key, '4893683748sj2i47');

        $this->assertEquals($string, $secretKeyMask->unmaskString($secretKeyMask->maskString($string)));
    }

    public function testEncodesAndDecodesFilePath()
    {
        $key = 'I am secret key.';
        $path = m::mock('Potato\MediaBundle\Accessor\Path', array(
            'parseString' => null,
            'asString' => 'path'
        ));

        $file = m::mock('Potato\MediaBundle\Accessor\Path', array(
            'asString' => 'file',
            'setName' => null
        ));

        $filePath = m::mock('Potato\MediaBundle\Accessor\Location\FilePath', array(
            'getPath' => $path,
            'getFile' => $file,
            'setPath' => null,
            'setFile' => null,
            'asString' => 'pathfile'
        ));

        $secretKeyMask = new SecretKeyMask($key, '4893683748sj2i47');

        $this->assertEquals($filePath, $secretKeyMask->unmaskFilePath($secretKeyMask->maskFilePath($filePath)));
    }
}