<?php

namespace Potato\MediaBundle\Tests\Element;

use \Mockery as m;
use Potato\MediaBundle\Element\Image;

class ImageTest extends \PHPUnit_Framework_TestCase
{
    public function testGettingMime()
    {
        $pathString = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;
        $fileString = 'potato.jpg';

        $path = m::mock('Potato\MediaBundle\Accessor\Location\Path', array(
            'getName' => $pathString,
            'asString' => $pathString
        ));

        $file = m::mock('Potato\MediaBundle\Accessor\Location\File', array(
            'getName' => $fileString,
            'asString' => $fileString,
            'getTmpFile' => $pathString.DIRECTORY_SEPARATOR.$fileString
        ));

        $image = new Image($path, $file);

        $this->assertEquals('image/jpeg', $image->getMime());
    }

    public function testGettingExtensionFromMime()
    {
        $pathString = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;
        $fileString = 'potato.jpg';

        $path = m::mock('Potato\MediaBundle\Accessor\Location\Path', array(
            'getName' => $pathString,
            'asString' => $pathString
        ));

        $file = m::mock('Potato\MediaBundle\Accessor\Location\File', array(
            'getName' => $fileString,
            'asString' => $fileString,
            'getTmpFile' => $pathString.DIRECTORY_SEPARATOR.$fileString
        ));

        $image = new Image($path, $file);

        $this->assertEquals('jpg', $image->getExtensionFromMime());
    }

    public function testGetWidthAndHeight()
    {
        $pathString = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;
        $fileString = 'potato.jpg';

        $path = m::mock('Potato\MediaBundle\Accessor\Location\Path', array(
            'getName' => $pathString,
            'asString' => $pathString
        ));

        $file = m::mock('Potato\MediaBundle\Accessor\Location\File', array(
            'getName' => $fileString,
            'asString' => $fileString,
            'getTmpFile' => $pathString.DIRECTORY_SEPARATOR.$fileString
        ));

        $image = new Image($path, $file);

        $this->assertGreaterThan(0, $image->getWidth());
        $this->assertGreaterThan(0, $image->getHeight());
    }

}