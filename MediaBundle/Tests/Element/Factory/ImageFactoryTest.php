<?php

namespace Potato\MediaBundle\Tests\Element\Factory;

use Potato\MediaBundle\Element\Factory\ImageFactory;

use \Mockery as m;

class ImageFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatesImageFromString()
    {
        $filePathFactory = m::mock('Potato\MediaBundle\Accessor\Location\FilePathFactory', array(
            'createFromString' => m::mock(
                'Potato\MediaBundle\Tests\Element\Image',
                array(
                    'getPath' => m::mock('Potato\MediaBundle\Accessor\Location\Path'),
                    'getFile' => m::mock('Potato\MediaBundle\Accessor\Location\File'),
                )
            )
        ));

        $imageFactory = new ImageFactory($filePathFactory);

        $this->assertInstanceOf(
            'Potato\MediaBundle\Element\Image',
            $imageFactory->createFromPathString('string/path')
        );
    }

    public function testCreatesImageFromPath()
    {
        $filePathFactory = m::mock('Potato\MediaBundle\Accessor\Location\FilePathFactory', array(
            'createFromString' => m::mock(
                'Potato\MediaBundle\Tests\Element\Image',
                array(
                    'getPath' => m::mock('Potato\MediaBundle\Accessor\Location\Path'),
                    'getFile' => m::mock('Potato\MediaBundle\Accessor\Location\File'),
                )
            )
        ));

        $filePath = m::mock('Potato\MediaBundle\Accessor\Location\FilePath', array(
            'getPath' => m::mock('Potato\MediaBundle\Accessor\Location\Path'),
            'getFile' => m::mock('Potato\MediaBundle\Accessor\Location\File'),
        ));

        $imageFactory = new ImageFactory($filePathFactory);

        $this->assertInstanceOf(
            'Potato\MediaBundle\Element\Image',
            $imageFactory->createFromFilePath($filePath)
        );
    }
}