<?php

namespace Potato\MediaBundle\Tests\Element;

use \Mockery as m;

class AbstractElementTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldSetName()
    {
        $path = m::mock('Potato\MediaBundle\Accessor\Location\Path');
        $file = m::mock('Potato\MediaBundle\Accessor\Location\File');

        $element = $this->getMockForAbstractClass(
            'Potato\MediaBundle\Element\AbstractElement',
            array($path, $file)
        );

        $this->assertEquals($path, $element->getPath());
        $this->assertEquals($file, $element->getFile());
    }
}