<?php

namespace Potato\MediaBundle\Tests\Twig;

use \Mockery as m;
use Potato\MediaBundle\Twig\PathExtension;

class PathExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testMasksString()
    {
        $mask = m::mock('Potato\MediaBundle\PathMask\PathMaskInterface');

        $mask->shouldReceive('maskString')->atLeast(1);

        $pathExtension = new PathExtension($mask);
        $pathExtension->mask('test');
    }

    public function tearDown()
    {
        m::close();
    }
}