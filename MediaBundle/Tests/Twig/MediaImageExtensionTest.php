<?php

namespace Potato\MediaBundle\Tests\Twig;

use \Mockery as m;
use Potato\MediaBundle\Twig\MediaImageExtension;

class MediaImageExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testReturnsPath()
    {
        $router = m::mock('Potato\MediaBundle\Routing\Image', array(
            'get' => 'route for file'
        ));

        $imageExtension = new MediaImageExtension($router);

        $this->assertContains('route for file', $imageExtension->image('dummy'));
    }
}