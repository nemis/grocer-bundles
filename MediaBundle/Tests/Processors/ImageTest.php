<?php

namespace Potato\MediaBundle\Tests\Processor;

use \Mockery as m;

class ImageTest extends \PHPUnit_Framework_TestCase
{
    private function getConstructorArgumentsMocks()
    {
        $imageManipulator = m::mock('Potato\MediaBundle\Manipulation\Image', array(
            'convert' => null,
            'resize' => null
        ));

        $imageFactory = m::mock('Potato\MediaBundle\Element\Factory\ImageFactory', array(
            'createFromPathString' => m::mock(
                'Potato\MediaBundle\Element\Image',
                array(
                    'getExtensionFromMime' => 'jpg',
                    'getFile' => m::mock(
                        'Potato\MediaBundle\Accessor\Location\File',
                        array(
                            'getName' => 'oldname',
                            'asString' => 'oldname',
                            'setContents' => null
                        )
                    ),
                    'getPath' => m::mock(
                        'Potato\MediaBundle\Accessor\Location\Path',
                        array(
                            'getName' => 'oldpathname',
                            'asString' => 'oldpathname'
                        )
                    ),
                    'setPath' => null,
                    'setFile' => null
                )

            )
        ));

        $accessor = m::mock('Potato\MediaBundle\Accessor\AccessorInterface', array(
            'createPathIfNotExists' => null,
            'store' => null
        ));

        $pathTranslator = m::mock('Potato\MediaBundle\PathTranslation\EntityTranslator', array(
           'translate' => m::mock(
               'Potato\MediaBundle\Accessor\Location\FilePath',
               array(
                   'getPath' => m::mock(
                       'Potato\MediaBundle\Accessor\Location\Path',
                       array(
                          'getName' => 'newname',
                          'asString' => 'newname'
                       )
                   ),
                   'getFile' => m::mock(
                       'Potato\MediaBundle\Accessor\Location\File',
                       array(
                           'getName' => 'newname',
                           'asString' => 'newname'
                       )
                   ),
                   'asString' => 'newfilepath'
               )
           )
        ));

        $pathFactory = m::mock('Potato\MediaBundle\Accessor\Location\FilePathFactory', array(
            'createFromString' => m::mock(
                'Potato\MediaBundle\Accessor\Location\FilePath',
                array(
                    'getPath' => m::mock('Potato\MediaBundle\Accessor\Location\Path'),
                    'getFile' => m::mock('Potato\MediaBundle\Accessor\Location\File')
                )
            ),
            'createFromPathAndFile'=> m::mock(
                'Potato\MediaBundle\Accessor\Location\FilePath',
                array(
                    'getPath' => m::mock('Potato\MediaBundle\Accessor\Location\Path'),
                    'getFile' => m::mock('Potato\MediaBundle\Accessor\Location\File')
                )
            )
        ));

        return array(
            'imageManipulator' => $imageManipulator,
            'imageFactory' => $imageFactory,
            'accessor' => $accessor,
            'pathTranslator' => $pathTranslator,
            'pathFactory' => $pathFactory
        );
    }

    public function testResizesImage()
    {
        $entity = new \StdClass();
        $entity->id = 2;
        $entity->image = __DIR__.'/../potato.jpg';

        $annotation = m::mock('Potato\MediaBundle\Annotation\Image', array(
            'maxWidth' => 100,
            'maxHeight' => 100
        ));

        $constructorArguments = $this->getConstructorArgumentsMocks();

        $constructorArguments['imageManipulator']->shouldReceive('resize')->atLeast(1);

        $constructorArguments['accessor']->shouldReceive('load')->atLeast(1);
        $constructorArguments['accessor']->shouldReceive('store')->atLeast(1);

        $reflection = new \ReflectionClass('Potato\MediaBundle\Processors\Image');

        $imageProcessor = $reflection->newInstanceArgs($constructorArguments);

        $imageProcessor->processPersist($annotation, $entity, 'image');
    }

    public function testMovesImage()
    {
        $entity = m::mock('\StdClass');
        $entity->id = 2;
        $entity->image = __DIR__.'/../potato.jpg';

        $entity->shouldReceive('setImage')->atLeast(1);

        $annotation = m::mock('Potato\MediaBundle\Annotation\Image');

        $constructorArguments = $this->getConstructorArgumentsMocks();

        $constructorArguments['accessor']->shouldReceive('load')->atLeast(1);
        $constructorArguments['accessor']->shouldReceive('store')->atLeast(1);

        $reflection = new \ReflectionClass('Potato\MediaBundle\Processors\Image');

        $imageProcessor = $reflection->newInstanceArgs($constructorArguments);

        $imageProcessor->processPersist($annotation, $entity, 'image');
    }

    public function testConvertsImage()
    {
        $entity = m::mock('\StdClass');
        $entity->id = 2;
        $entity->image = __DIR__.'/../potato.jpg';

        $entity->shouldReceive('setImage')->atLeast(1);

        $annotation = m::mock('Potato\MediaBundle\Annotation\Image');

        $constructorArguments = $this->getConstructorArgumentsMocks();

        $constructorArguments['imageManipulator']->shouldReceive('convert')->atLeast(1);

        $reflection = new \ReflectionClass('Potato\MediaBundle\Processors\Image');

        $imageProcessor = $reflection->newInstanceArgs($constructorArguments);

        $imageProcessor->processPersist($annotation, $entity, 'image');
    }

    public function testIsCroping()
    {
        $entity = m::mock('\StdClass');
        $entity->id = 2;

        $value = __DIR__.'/../potato.jpg';

        $annotation = m::mock('Potato\MediaBundle\Annotation\Image');

        $cropData = m::mock('Potato\MediaBundle\Manipulation\Data\Crop', array(
            'getX' => 10,
            'getY' => 10,
            'getWidth' => 100,
            'getHeight' => 100,
            'getOriginalValue' => $value,
            '__toString' => $value
        ));

        $entity->image = $cropData;

        $constructorArguments = $this->getConstructorArgumentsMocks();

        $constructorArguments['imageManipulator']->shouldReceive('crop')->times(1);

        $reflection = new \ReflectionClass('Potato\MediaBundle\Processors\Image');

        $imageProcessor = $reflection->newInstanceArgs($constructorArguments);

        $imageProcessor->processPersist($annotation, $entity, 'image');
    }

    public function testRemovesImage()
    {
        $entity = m::mock('\StdClass');
        $entity->id = 2;

        $value = __DIR__.'/../potato.jpg';

        $annotation = m::mock('Potato\MediaBundle\Annotation\Image');

        $cropData = m::mock('Potato\MediaBundle\Manipulation\Data\Crop', array(
            'getX' => 10,
            'getY' => 10,
            'getWidth' => 100,
            'getHeight' => 100,
            'getOriginalValue' => $value,
            '__toString' => $value
        ));

        $entity->image = $cropData;

        $constructorArguments = $this->getConstructorArgumentsMocks();

        $constructorArguments['accessor']->shouldReceive('deleteFile')->times(1);

        $reflection = new \ReflectionClass('Potato\MediaBundle\Processors\Image');

        $imageProcessor = $reflection->newInstanceArgs($constructorArguments);

        $imageProcessor->processRemove($annotation, $entity, 'image');
    }

    protected function tearDown()
    {
        m::close();
    }
}