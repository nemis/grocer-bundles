<?php

namespace Potato\MediaBundle\Tests\Processos\Container;

use \Mockery as m;
use Potato\MediaBundle\Processors\Container\Container;

class ContainerTest extends \PHPUnit_Framework_TestCase
{
    public function testCallsSubProcessors()
    {
        $processor1 = m::mock('Potato\MediaBundle\Processors\ProcessorInterface');
        $processor1->shouldReceive('processPersist')->once();
        $processor1->shouldReceive('processRemove')->once();

        $processor2 = m::mock('Potato\MediaBundle\Processors\ProcessorInterface');
        $processor2->shouldReceive('processPersist')->once();
        $processor2->shouldReceive('processRemove')->once();

        $processor3 = m::mock('Potato\MediaBundle\Processors\ProcessorInterface');
        $processor3->shouldReceive('processPersist')->once();
        $processor3->shouldReceive('processRemove')->once();

        $container = new Container();

        $container->addProcessor($processor1);
        $container->addProcessor($processor2);
        $container->addProcessor($processor3);

        $annotation = m::mock('Potato\MediaBundle\Annotation\AnnotationInterface');
        $entity = m::mock('stdClass');
        $property = m::mock('stdClass');

        $container->processPersist($annotation, $entity, $property);
        $container->processRemove($annotation, $entity, $property);
    }

    public function tearDown()
    {
        m::close();
    }
}