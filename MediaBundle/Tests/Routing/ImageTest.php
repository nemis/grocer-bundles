<?php

namespace Potato\MediaBundle\Tests\Routing;

use \Mockery as m;
use Potato\MediaBundle\Routing\Image;

class ImageTest extends \PHPUnit_Framework_TestCase
{
    public function testImageRoute()
    {
        $router = m::mock('Symfony\Bundle\FrameworkBundle\Routing\Router');

        $router->shouldReceive('generate')->atLeast(1)->andReturn('test');

        $pathMask = m::mock('Potato\MediaBundle\PathMask\PathMaskInterface');

        $pathMask->shouldReceive('maskString')->atLeast(1);

        $media = new Image($pathMask, $router);

        $src = $media->get('/home/test/test/');

        $this->assertNotEmpty($src);
    }

    public function tearDown()
    {
        m::close();
    }
}
