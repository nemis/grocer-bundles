<?php

namespace Potato\MediaBundle\Tests\Validator\Constraints;

use \Mockery as m;
use Potato\MediaBundle\Validator\Constraints\MediaImageValidator;

class MediaImageValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function testValidatesStringIfFileExists()
    {
        $accessor = m::mock('Potato\MediaBundle\Accessor\AccessorInterface', array(
            'fileExists' => true
        ));

        $pathFactory = m::mock('Potato\MediaBundle\Accessor\Location\FilePathFactory', array(
            'createFromString' => m::mock(
                'Potato\MediaBundle\Accessor\Location\FilePath',
                array(
                    'getPath' => m::mock('Potato\MediaBundle\Accessor\Location\Path'),
                    'getFile' => m::mock('Potato\MediaBundle\Accessor\Location\File')
                )
            )
        ));

        $mediaImageValidator = new MediaImageValidator($accessor, $pathFactory);

        $constraint = m::mock('Potato\MediaBundle\Validator\Constraints\MediaImage');

        // should not throw exception
        try {
            $mediaImageValidator->validate('test.jpg', $constraint);
        } catch (\Exception $e) {
            $this->fail('Exception from validation.');
        }
    }
}