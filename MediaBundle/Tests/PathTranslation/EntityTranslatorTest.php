<?php

namespace Potato\MediaBundle\Tests\PathTranslation;

use Potato\MediaBundle\PathTranslation\EntityTranslator;
use \Mockery as m;

class EntityTranslatorTest extends \PHPUnit_Framework_TestCase
{
    public function testTranslatingLocation()
    {
        $filePath = m::mock('Potato\MediaBundle\Accessor\Location\FilePath', array(
            'getFile' => m::mock(
                'Potato\MediaBundle\Accessor\Location\File',
                array(
                    'getExtension' => 'jpg',
                    'getContents' => '123',
                    'setContents' => null
                )
            )
        ));

        $expectedFilePath = m::mock('Potato\MediaBundle\Accessor\Location\FilePath', array(
            'getFile' => m::mock(
                'Potato\MediaBundle\Accessor\Location\File',
                array(
                    'getExtension' => 'jpg',
                    'getContents' => '123',
                    'setContents' => null
                )
            )
        ));

        $filePathFactory = m::mock('Potato\MediaBundle\Accessor\Location\FilePathFactory');

        $resultString = '';

        $filePathFactory
            ->shouldReceive('createFromString')
            ->andReturnUsing(function ($string) use ($expectedFilePath, & $resultString) {
                $resultString = $string;
                return $expectedFilePath;
            });

        $translator = new EntityTranslator('upload', $filePathFactory);

        $object = m::mock('StdClass');

        $object->shouldReceive('getId')->andReturn(2);

        $translator->translate($object, 'image', $filePath);

        $this->assertTrue(strstr($resultString, '2_image.jpg') > -1);
    }

    public function tearDown()
    {
        m::close();
    }
}