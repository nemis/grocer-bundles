<?php

namespace Potato\MediaBundle\Tests\PathTranslation;

class AbstractTranslatorTest extends \PHPUnit_Framework_TestCase
{
    public function testCreation()
    {
        $translator = $this->getMockForAbstractClass('Potato\MediaBundle\PathTranslation\AbstractTranslator', array(
            'upload',
            $this->getMock('Potato\MediaBundle\Accessor\Location\FilePathFactory')
        ));

        $this->assertInstanceOf('Potato\MediaBundle\PathTranslation\AbstractTranslator', $translator);
    }
}