<?php

namespace Potato\MediaBundle\Tests\Manipulation\Data;

use Potato\MediaBundle\Manipulation\Data\Crop;

class CropTest extends \PHPUnit_Framework_TestCase
{
    public function testKeepsData()
    {
        $crop = new Crop(1, 2, 3, 4);

        $this->assertEquals(1, $crop->getX());
        $this->assertEquals(2, $crop->getY());
        $this->assertEquals(3, $crop->getWidth());
        $this->assertEquals(4, $crop->getHeight());

        $crop->setOriginalValue('test');

        $this->assertEquals('test', $crop->getOriginalValue());
    }
}
