<?php

namespace Potato\MediaBundle\Tests\Manipulation\Data;

use Potato\MediaBundle\Manipulation\Data\DataFactory;

class DataFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatesCrop()
    {
        $dataFactory = new DataFactory();
        $cropData = $dataFactory->createCropData(1, 2, 3, 4);

        $this->assertInstanceOf('Potato\MediaBundle\Manipulation\Data\Crop', $cropData);
    }
}