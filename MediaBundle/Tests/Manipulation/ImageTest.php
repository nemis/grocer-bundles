<?php

namespace Potato\MediaBundle\Tests\Manipulation;

use Potato\MediaBundle\Manipulation\Image;
use \Mockery as m;

class ImageTest extends \PHPUnit_Framework_TestCase
{
    public function testCallAdapterForResize()
    {
        $file = m::mock('Potato\MediaBundle\Element\Image');
        $adapter = m::mock('Potato\MediaBundle\Manipulation\Adapters\Image\ImageAdapterInterface');

        $adapter->shouldReceive('resize')->times(1);

        $imageSize = new Image($adapter);

        $imageSize->resize($file, 100, 100);
    }

    public function testCallAdapterForConvert()
    {
        $file = m::mock('Potato\MediaBundle\Element\Image');
        $adapter = m::mock('Potato\MediaBundle\Manipulation\Adapters\Image\ImageAdapterInterface');

        $adapter->shouldReceive('convert')->times(1);

        $imageSize = new Image($adapter);

        $imageSize->convert($file, 'jpg');
    }

    public function testCallAdapterForCrop()
    {
        $file = m::mock('Potato\MediaBundle\Element\Image');
        $adapter = m::mock('Potato\MediaBundle\Manipulation\Adapters\Image\ImageAdapterInterface');

        $adapter->shouldReceive('crop')->times(1);

        $imageSize = new Image($adapter);

        $imageSize->crop($file, 0, 0, 100, 100);
    }

    public function tearDown()
    {
        m::close();
    }
}