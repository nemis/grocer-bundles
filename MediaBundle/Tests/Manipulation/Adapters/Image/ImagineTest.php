<?php

namespace Potato\MediaBundle\Tests\Manipulation\Adapters\Image;

use Potato\MediaBundle\Accessor\Location\File;
use Potato\MediaBundle\Element\Image;
use Potato\MediaBundle\Manipulation\Adapters\Image\Imagine;
use \Mockery as m;

class ImagineTest extends \PHPUnit_Framework_TestCase
{
    public function testResize()
    {
        if (!class_exists('\Imagine\Gd\Imagine')) {
            return; // if there is no imagine bundle, do not test it, probably may need to inject imagine into adapter
        }

        $file = new File('potato.jpg');
        $file->setContents(file_get_contents(__DIR__.'/../../../potato.jpg'));

        $oldSize = getimagesize(__DIR__.'/../../../potato.jpg');

        $imagineAdapter = new Imagine();

        $path = m::mock('Potato\MediaBundle\Accessor\Location\Path', array(
            'asString' => __DIR__.'/../../../'
        ));

        $image = new Image($path, $file);

        $maxWidth = 10;
        $maxHeight = 10;

        $imagineAdapter->resize($image, $maxWidth, $maxHeight);

        $resultFile = tempnam(sys_get_temp_dir(), 'testpotato.jpg');

        file_put_contents($resultFile, $file->getContents());

        $size = getimagesize($resultFile);

        $this->assertTrue($size[0] === $maxWidth || $size[1] === $maxHeight);
    }

    public function testCrop()
    {
        if (!class_exists('\Imagine\Gd\Imagine')) {
            return; // if there is no imagine bundle, do not test it, probably may need to inject imagine into adapter
        }

        $file = new File('potato.jpg');
        $file->setContents(file_get_contents(__DIR__.'/../../../potato.jpg'));

        $oldSize = getimagesize(__DIR__.'/../../../potato.jpg');

        $imagineAdapter = new Imagine();

        $path = m::mock('Potato\MediaBundle\Accessor\Location\Path', array(
            'asString' => __DIR__.'/../../../'
        ));

        $image = new Image($path, $file);

        $maxWidth = 10;
        $maxHeight = 10;

        $imagineAdapter->crop($image, 2, 2, $maxWidth, $maxHeight);

        $resultFile = tempnam(sys_get_temp_dir(), 'testpotato.jpg');

        file_put_contents($resultFile, $file->getContents());

        $size = getimagesize($resultFile);

        $ratio = $oldSize[0] / $oldSize[1];

        $expectedWidth = 10;
        $expectedHeight = 10;

        $this->assertEquals($expectedWidth, $size[0]);
        $this->assertEquals($expectedHeight, $size[1]);
    }
}