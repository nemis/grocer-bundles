<?php

namespace Potato\MediaBundle\Tests\Accessor\Location;

use \Mockery as m;
use Potato\MediaBundle\Accessor\Location\FilePath;

class FilePathTest extends \PHPUnit_Framework_TestCase
{
    public function testCreationWithPathAndFile()
    {
        $path = m::mock('Potato\MediaBundle\Accessor\Location\Path');
        $file = m::mock('Potato\MediaBundle\Accessor\Location\File');

        $filePath = new FilePath($path, $file);

        $this->assertEquals($path, $filePath->getPath());
        $this->assertEquals($file, $filePath->getFile());
    }
}