<?php

namespace Potato\MediaBundle\Tests\Accessor\Location;

use Potato\MediaBundle\Accessor\Location\Path;

class PathTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateAString()
    {
        $path = new Path('folder1');

        $path->addSubPath('folder2')->addSubPath('folder3');

        $this->assertEquals('folder1/folder2/folder3', $path->asString());
    }

    public function testCreateFromString()
    {
        $path = new Path();
        $path->parseString('folder1/folder2/folder3');

        $this->assertEquals('folder1/folder2/folder3', $path->asString());
    }

    public function testReturnsLastElement()
    {
        $path = new Path();
        $path->parseString('folder1/folder2/folder3');

        $this->assertEquals('folder3', $path->getLastElement());
    }
}