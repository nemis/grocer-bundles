<?php

namespace Potato\MediaBundle\Tests\Accessor\Location;

use Potato\MediaBundle\Accessor\Location\File;

class FileTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatesName()
    {
        $element = new File('cute_girl.jpg');

        $this->assertEquals('cute_girl.jpg', $element->getName());
    }

    public function testReturnsExtension()
    {
        $element = new File();

        $element->setName('cuteGirl2.jpg');

        $this->assertEquals('jpg', $element->getExtension());
    }

    public function testReturnsContents()
    {
        $element = new File();

        $element->setContents('amelka');

        $this->assertEquals('amelka', $element->getContents());
    }

    public function testTmpContents()
    {
        $element = new File();

        $element->setContents('amelka');
        $file = $element->getTmpFile();

        $this->assertEquals('amelka', file_get_contents($file));
    }
}