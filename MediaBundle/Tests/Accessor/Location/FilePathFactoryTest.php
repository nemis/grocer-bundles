<?php

namespace Potato\MediaBundle\Tests\Accessor\Location;

use \Mockery as m;
use Potato\MediaBundle\Accessor\Location\FilePathFactory;

class FilePathFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreationWithPathAndFile()
    {
        $filePathFactory = new FilePathFactory();

        $filePath = $filePathFactory->createFromString('test/test/test/test.jpg');

        $this->assertEquals('test.jpg', $filePath->getFile()->getName());
        $this->assertEquals('test/test/test', $filePath->getPath()->asString());
    }

    public function testCreatingFromPathAndFile()
    {
        $file = $this->getMock('Potato\MediaBundle\Accessor\Location\File');
        $path = $this->getMock('Potato\MediaBundle\Accessor\Location\Path');

        $filePathFactory = new FilePathFactory();

        $filePath = $filePathFactory->createFromPathAndFile($path, $file);

        $this->assertEquals($path, $filePath->getPath());
        $this->assertEquals($file, $filePath->getFile());
    }
}