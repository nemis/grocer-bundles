<?php

namespace Potato\MediaBundle\Tests\Accessors;

use \Mockery as m;
use Potato\MediaBundle\Accessor\FileSystem;
use Potato\MediaBundle\Accessor\Location\File;
use Potato\MediaBundle\Accessor\Location\Path;

class FileSystemTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldStoreAndRetrieveFile()
    {
        $fileSystemAccessor = new FileSystem();

        $path = new Path();
        $path->parseString(sys_get_temp_dir());

        $file = new File('agatka.txt');

        $file->setContents($content = 'How to make something from paper.');

        $fileSystemAccessor->store($path, $file);

        $contentResult = $fileSystemAccessor->load($path, $file)->getContents();

        $this->assertEquals($content, $contentResult);

        unlink(sys_get_temp_dir().DIRECTORY_SEPARATOR.'agatka.txt');
    }

    public function testCreatingPath()
    {
        $fileSystemAccessor = new FileSystem();

        $dir = sys_get_temp_dir().'/new/path/for/test';
        $subdir = $dir.DIRECTORY_SEPARATOR.'test';

        if (is_dir($subdir)) {
            @rmdir($subdir);
        }

        if (is_dir($dir)) {
            @rmdir($dir);
        }

        $path = new Path();
        $path->parseString($dir);

        $fileSystemAccessor->createPath($path);

        $this->assertTrue(is_dir($dir));

        $path->getLastElement()->addSubPath('test');

        $fileSystemAccessor->createPathIfNotExists($path);

        $this->assertTrue(is_dir($subdir));

        rmdir($dir.DIRECTORY_SEPARATOR.'test');
        rmdir($dir);
    }

    public function testDeletingPath()
    {
        $pathString = sys_get_temp_dir().DIRECTORY_SEPARATOR.'testfolder'.uniqid();

        mkdir($pathString);

        $path = new Path();
        $path->parseString($pathString);

        $fileSystemAccessor = new FileSystem();

        $fileSystemAccessor->deletePath($path);

        $this->assertFalse(is_dir($pathString));
    }

    public function testDeletingFile()
    {
        $fileSystemAccessor = new FileSystem();

        $path = new Path();
        $path->parseString(sys_get_temp_dir());

        $file = new File('agatka.txt');

        touch($f = sys_get_temp_dir().DIRECTORY_SEPARATOR.'agatka.txt');

        $fileSystemAccessor->deleteFile($path, $file);

        $this->assertFileNotExists(sys_get_temp_dir().DIRECTORY_SEPARATOR.'agatka.txt');
    }

    public function testFileExists()
    {
        $fileSystemAccessor = new FileSystem();

        $path = new Path();
        $path->parseString(sys_get_temp_dir());

        $file = new File('agatka.txt');

        touch($f = sys_get_temp_dir().DIRECTORY_SEPARATOR.'agatka.txt');

        $this->assertTrue($fileSystemAccessor->fileExists($path, $file));
    }
}