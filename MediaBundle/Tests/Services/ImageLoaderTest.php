<?php

namespace Potato\MediaBundle\Tests\Services;

use \Mockery as m;
use Potato\MediaBundle\Services\ImageLoader;

class ImageLoaderTest extends \PHPUnit_Framework_TestCase
{
    public function testReturnsResizedImage()
    {
        $imageManipulator = m::mock('Potato\MediaBundle\Manipulation\Image', array(
            'convert' => null,
            'resize' => null
        ));

        $imageFactory = m::mock('Potato\MediaBundle\Element\Factory\ImageFactory', array(
            'createFromPathString' => m::mock(
                'Potato\MediaBundle\Element\Image',
                array(
                    'getExtensionFromMime' => 'jpg',
                    'getFile' => m::mock(
                        'Potato\MediaBundle\Accessor\Location\File',
                        array(
                            'getName' => 'oldname',
                            'asString' => 'oldname',
                            'setContents' => null
                        )
                    ),
                    'getPath' => m::mock(
                        'Potato\MediaBundle\Accessor\Location\Path',
                        array(
                            'getName' => 'oldpathname',
                            'asString' => 'oldpathname'
                        )
                    ),
                    'setPath' => null,
                    'setFile' => null
                )
            )
        ));

        $accessor = m::mock('Potato\MediaBundle\Accessor\AccessorInterface', array(
            'createPathIfNotExists' => null,
            'store' => null,
            'load' => null
        ));

        $imageLoader = new ImageLoader($accessor, $imageManipulator, $imageFactory);

        $imageLoader->loadFromString('test/test/image.jpg');
    }

    public function tearDown()
    {
        m::close();
    }
}