<?php

namespace Potato\MediaBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\Annotations\Reader;
use Potato\MediaBundle\Annotation\Image as ImageAnnotation;
use Potato\MediaBundle\Processors\ProcessorInterface;

/**
 * Class DoctrineListener
 * @package Potato\MediaBundle\EventListener
 */
class DoctrineListener
{
    const ACTION_PERSIST = 1;
    const ACTION_REMOVE = 2;

    /**
     * @var \Doctrine\Common\Annotations\Reader
     */
    private $reader;
    /**
     * @var ProcessorInterface[]
     */
    private $processors;

    /**
     * @param Reader $reader
     */
    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * @param ProcessorInterface $processor
     */
    public function addProcessor(ProcessorInterface $processor)
    {
        $this->processors[] = $processor;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->process($args, self::ACTION_PERSIST);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->process($args, self::ACTION_PERSIST);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $this->process($args, self::ACTION_REMOVE);
    }

    /**
     * @param LifecycleEventArgs $args
     * @param string $action
     */
    private function process(LifecycleEventArgs $args, $action)
    {
        $method = $action == self::ACTION_PERSIST ? 'processPersist' : 'processRemove';

        $entity = $args->getEntity();

        $object = new \ReflectionObject($entity);
        $properties = $object->getProperties();

        foreach ($properties as $property) {
            $annotations = $this->reader->getPropertyAnnotations($property);

            foreach ($annotations as $annotation) {
                if ($annotation instanceof ImageAnnotation) {
                    foreach ($this->processors as $processor) {
                        $processor->$method($annotation, $entity, $property->getName());
                    }
                }
            }
        }
    }
}